# BridgeFighters

```
<=====================================[ Terms of usage ]=====================================>
This work is licensed under CC BY NC 4.0. You are free to:
- Share - copy and redistribute the material in any medium or format,
- Adapt - remix, transform, and build upon the material
Under the following terms:
- Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
  You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
  You are not allowed to remove any attribution text from this code.
  You are not allowed to change this terms.
- NonCommercial - You may not use the material for commercial purposes.
If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode

This was developed by MrLeaw.
<=================[ Thanks for being interested in my work. It means a lot. ]=================>
```

![CC BY NC 4.0 Logo](https://i.creativecommons.org/l/by-nc/4.0/88x31.png "CC BY NC 4.0 Logo")

Please respect the above terms.
You are allowed to use this plugin on your Minecraft server, if it is a NonCommercial server. _(if you are not earning money with it)_

There will be a documentation of all available commands and all the features as well as a getting started guide here soon.
Stay tuned.

[Click here to get to the wiki](https://gitlab.com/MrLeaw/bridgefighter/-/wikis/home)