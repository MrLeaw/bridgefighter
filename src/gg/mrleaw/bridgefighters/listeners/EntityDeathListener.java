//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import gg.mrleaw.bridgefighters.utils.StatsManager;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

public class EntityDeathListener implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getEntity() instanceof Wither) {
            e.getDrops().clear();
            e.setDroppedExp(0);
            e.getEntity().getLocation().getWorld().dropItemNaturally(e.getEntity().getLocation(), new ItemStack(Material.GOLDEN_APPLE));
            TeamColor color = null;
            for (Map.Entry<TeamColor, Location> entry : Data.withers.entrySet()) {
                if (entry.getValue().equals(e.getEntity().getLocation())) {
                    color = entry.getKey();
                }
            }
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                if (teamObject.getTeamColor() == color) {
                    for (Player player : teamObject.getPlayers()) {
                        MultiLanguage.sendMessage(player,
                                Data.getPrefix() + "§cDein Wither wurde getötet!",
                                Data.getPrefix() + "§cYour Wither was killed!",
                                Data.getPrefix() + "§c¡Tu Wither fue asesinado!");
                    }
                } else {
                    for (Player player : teamObject.getPlayers()) {
                        MultiLanguage.sendMessage(player,
                                Data.getPrefix() + "§7Der Wither von Team " + color.getColor()
                                        + color.toString().toLowerCase().substring(0, 1).toUpperCase()
                                        + color.toString().toLowerCase().substring(1) + " §7wurde getötet!",
                                Data.getPrefix() + "§7The Wither of Team " + color.getColor()
                                        + color.toString().toLowerCase().substring(0, 1).toUpperCase()
                                        + color.toString().toLowerCase().substring(1) + " §7was killed!",
                                Data.getPrefix() + "§7El Wither de Team " + color.getColor()
                                        + color.toString().toLowerCase().substring(0, 1).toUpperCase()
                                        + color.toString().toLowerCase().substring(1) + " §7fue asesinado!");
                    }
                    MultiLanguage.sendHolo(Data.last_wither_hitter.get(color), e.getEntity().getLocation(), "+ §6100 Coins", 50);
                    Data.last_wither_hitter.get(color).playSound(Data.last_wither_hitter.get(color).getLocation(), Sound.LEVEL_UP, 10, 1);
                    StatsManager.addPoints(Data.last_wither_hitter.get(color).getUniqueId(), 20);
                    break;
                }
            }
        }
    }

}
