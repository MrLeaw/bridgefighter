//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.Daytime;
import gg.mrleaw.bridgefighters.utils.settings.Spawners;
import gg.mrleaw.bridgefighters.utils.settings.Teams;
import gg.mrleaw.bridgefighters.utils.settings.Villagers;
import org.bukkit.Location;

public class GameSettings {

    // VARIABLES
    private Daytime daytime;
    private Teams teams;
    private Villagers villagers;
    private Spawners spawners;
    private Location lobby_spawn;
    private String mapname;
    private String buildername;


    // CONSTRUCTOR
    public GameSettings() {
    }


    // GETTERS & SETTERS
    public Daytime getDaytime() {
        return daytime;
    }

    public void setDaytime(Daytime daytime) {
        this.daytime = daytime;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }

    public Villagers getVillagers() {
        return villagers;
    }

    public void setVillagers(Villagers villagers) {
        this.villagers = villagers;
    }

    public Spawners getSpawners() {
        return spawners;
    }

    public void setSpawners(Spawners spawners) {
        this.spawners = spawners;
    }

    public Location getLobby_spawn() {
        return lobby_spawn;
    }

    public void setLobby_spawn(Location lobby_spawn) {
        this.lobby_spawn = lobby_spawn;
    }

    public String getMapname() {
        return mapname;
    }

    public void setMapname(String mapname) {
        this.mapname = mapname;
    }

    public String getBuildername() {
        return buildername;
    }

    public void setBuildername(String buildername) {
        this.buildername = buildername;
    }
}
