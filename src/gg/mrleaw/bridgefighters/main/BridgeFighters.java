//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.main;

import gg.mrleaw.bridgefighters.commands.*;
import gg.mrleaw.bridgefighters.listeners.*;
import gg.mrleaw.bridgefighters.utils.*;
import gg.mrleaw.bridgefighters.utils.settings.Daytime;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import gg.mrleaw.bridgefighters.utils.settings.Teams;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityTypes;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

public class BridgeFighters extends JavaPlugin {

    /**
     * VARIABLES
     */
    private static BridgeFighters instance;
    private static GameSettings gameSettings = new GameSettings();
    private static ArrayList<MapSettings> maps = new ArrayList<>();
    private static GameState gameState;
    private static MapSettings selectedMap = null;
    public static ArrayList<TeamObject> teamObjects = new ArrayList<>();
    public static ArrayList<TeamObject> allPlayers = new ArrayList<>();


    // METHODS
    @Override
    public void onEnable() {
        instance = this;
        if (!Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
            getLogger().severe("*** HolographicDisplays is not installed or not enabled. ***");
            getLogger().severe("*** This plugin will be disabled. ***");
            this.setEnabled(false);
            return;
        }
        FileManager.setDefaultMySQLConfig();
        FileManager.getValues();
        if(!Data.cc_accepted) {
            Bukkit.getConsoleSender().sendMessage("§c<=====================================[ §6BridgeFighters -- Terms§c ]=====================================>");
            Bukkit.getConsoleSender().sendMessage("§cThis work is licensed under CC BY NC 4.0. You are free to:");
            Bukkit.getConsoleSender().sendMessage("§c- §lShare§r§c - copy and redistribute the material in any medium or format,");
            Bukkit.getConsoleSender().sendMessage("§c- §lAdapt§r§c - remix, transform, and build upon the material");
            Bukkit.getConsoleSender().sendMessage("§cUnder the following terms:");
            Bukkit.getConsoleSender().sendMessage("§c- §lAttribution§r§c - You must give appropriate credit, provide a link to the license, and indicate if changes were made.");
            Bukkit.getConsoleSender().sendMessage("§c  You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.");
            Bukkit.getConsoleSender().sendMessage("§c  You are not allowed to remove any attribution text from this code.");
            Bukkit.getConsoleSender().sendMessage("§c  You are not allowed to change this terms.");
            Bukkit.getConsoleSender().sendMessage("§c- §lNonCommercial§r§c - You may not use the material for commercial purposes.");
            Bukkit.getConsoleSender().sendMessage("§cIf you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode");
            Bukkit.getConsoleSender().sendMessage("§c<===============================[ type §e/bfaccept§c to accept the terms. ]===============================>");
        }
        FileManager.createDefaultSignValues();
        FileManager.getSignValues();
        registerCustomEntities();
        FileManager.readGameSettings();
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new EntitySpawnListener(), this);
        pm.registerEvents(new PlayerChatListener(), this);
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new PlayerDamangeListener(), this);
        pm.registerEvents(new FoodLevelChangeListener(), this);
        pm.registerEvents(new PressurePlateListener(), this);
        pm.registerEvents(new ScoreboardObject(), this);
        pm.registerEvents(new TeamSelectListener(), this);
        pm.registerEvents(new BlockBreakListener(), this);
        pm.registerEvents(new InventoryClickListener(), this);
        pm.registerEvents(new PlayerInteractEntityListener(), this);
        pm.registerEvents(new TeamShopListener(), this);
        pm.registerEvents(new MiddleShopListener(), this);
        pm.registerEvents(new JumpNRunListener(), this);
        pm.registerEvents(new WeatherChangeListener(), this);
        pm.registerEvents(new PlayerQuitListener(), this);
        pm.registerEvents(new EntityDamageListener(), this);
        pm.registerEvents(new BlockPlaceListener(), this);
        pm.registerEvents(new PlayerRespawnListener(), this);
        pm.registerEvents(new PlayerDeathListener(), this);
        pm.registerEvents(new EntityDeathListener(), this);
        pm.registerEvents(new PlayerMoveListener(), this);
        pm.registerEvents(new PrepareItemCraftListener(), this);
        pm.registerEvents(new DisableUproot(), this);
        pm.registerEvents(new ForcemapListener(), this);
        pm.registerEvents(new PlayerPickupItemListener(), this);
        pm.registerEvents(new PlayerDropItemListener(), this);
        pm.registerEvents(new EntityExplodeListener(), this);
        pm.registerEvents(new PlayerFishListener(), this);
        pm.registerEvents(new ServerPingListener(), this);
        for (TeamColor teamColor : Teams.getTeams()) {
            TeamObject teamObject = new TeamObject(teamColor, new ArrayList<>());
            teamObjects.add(teamObject);
            allPlayers.add(teamObject);
        }
        ScoreboardObject.registerScoreboard();
        getCommand("setup").setExecutor(new CMD_setup());
        getCommand("start").setExecutor(new CMD_start());
        getCommand("stats").setExecutor(new CMD_stats());
        getCommand("forcemap").setExecutor(new CMD_forcemap());
        getCommand("info").setExecutor(new CMD_info());
        getCommand("addsign").setExecutor(new CMD_addsign());
        getCommand("setholo").setExecutor(new CMD_setholo());
        getCommand("bfaccept").setExecutor(new CMD_bfaccept());
        getCommand("help").setExecutor(new BlockedCMDS());
        getCommand("me").setExecutor(new BlockedCMDS());
        getCommand("about").setExecutor(new BlockedCMDS());
        getCommand("tell").setExecutor(new BlockedCMDS());
        WaterDamageManager.start();
        Bukkit.getWorlds().forEach(world -> world.getEntities().forEach(entity -> {
            if (!(entity instanceof Player)) {
                entity.remove();
            }
        }));
        Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§aBridgeFighters v"+this.getDescription().getVersion()+" started! ");
        Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "by §6§lMrLeaw");
        Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§bINFO: This is a development build which might contain bugs and lag sources and could lead to unexpected shutdowns!");
        for (World world : Bukkit.getWorlds()) {
            world.setStorm(false);
        }
        if (getGameSettings().getDaytime() == Daytime.DAY) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
                for (World world : Bukkit.getWorlds()) {
                    world.setTime(2000);
                }
            }, 1, 1);
        } else if (getGameSettings().getDaytime() == Daytime.NIGHT) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
                for (World world : Bukkit.getWorlds()) {
                    world.setTime(14000);
                }
            }, 1, 1);
        }
        MySQL.connect();
        MySQL.createTable();
        MySQL.disableAutoDisconnect();
        for(World world : Bukkit.getServer().getWorlds()){
            world.setAutoSave(false);
        }
    }

    private void registerCustomEntities() {
        try {
            Field c = EntityTypes.class.getDeclaredField("c");
            Field f = EntityTypes.class.getDeclaredField("f");
            c.setAccessible(true);
            f.setAccessible(true);
            ((Map<Class<? extends EntityInsentient>, String>) c.get(null)).put(CustomWither.class, "Wither");
            ((Map<Class<? extends EntityInsentient>, Integer>) f.get(null)).put(CustomWither.class, 64);
            ((Map<Class<? extends EntityInsentient>, String>) c.get(null)).put(CustomVillager.class, "Villager");
            ((Map<Class<? extends EntityInsentient>, Integer>) f.get(null)).put(CustomVillager.class, 120);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        MySQL.close();
    }

    // GETTERS & SETTERS
    public static GameSettings getGameSettings() {
        return gameSettings;
    }

    public static void setGameSettings(GameSettings gameSettings) {
        BridgeFighters.gameSettings = gameSettings;
    }

    public static BridgeFighters getInstance() {
        return instance;
    }

    public static GameState getGameState() {
        return gameState;
    }

    public static void setGameState(GameState gameState) {
        BridgeFighters.gameState = gameState;
    }

    public static ArrayList<MapSettings> getMaps() {
        return maps;
    }

    public static MapSettings getSelectedMap() {
        return selectedMap;
    }

    public static void setSelectedMap(MapSettings selectedMap) {
        BridgeFighters.selectedMap = selectedMap;
    }
}
