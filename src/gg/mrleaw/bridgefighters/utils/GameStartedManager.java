//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class GameStartedManager {

    public static void setTeams() {
        ArrayList<Player> noteam = new ArrayList<>(Bukkit.getOnlinePlayers());
        for (TeamObject teamObject : BridgeFighters.teamObjects) {
            for (Player player : teamObject.getPlayers()) {
                noteam.remove(player);
            }
        }
        for (Player player : noteam) {
            ArrayList<Integer> sizes = new ArrayList<>();
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                sizes.add(teamObject.getPlayers().size());
            }
            int smallest = Collections.min(sizes);
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                if (teamObject.getPlayers().size() == smallest) {
                    teamObject.getPlayers().add(player);
                    ScoreboardObject.setTeam(player, teamObject.getTeamColor());
                    MultiLanguage.sendMessage(player,
                            Data.getPrefix() + "§7Da du kein Team gewählt hast, wurdest du Team " + teamObject.getTeamColor().getColor() + teamObject.getTeamColor().toString().toLowerCase().substring(0, 1).toUpperCase() + teamObject.getTeamColor().toString().toLowerCase().substring(1) + "§7 zugewiesen!",
                            Data.getPrefix() + "§7Since you did not choose a team, you were assigned to Team " + teamObject.getTeamColor().getColor() + teamObject.getTeamColor().toString().toLowerCase().substring(0, 1).toUpperCase() + teamObject.getTeamColor().toString().toLowerCase().substring(1) + "§7!",
                            Data.getPrefix() + "§7Como no elegiste un equipo, fuiste asignado al equipo " + teamObject.getTeamColor().getColor() + teamObject.getTeamColor().toString().toLowerCase().substring(0, 1).toUpperCase() + teamObject.getTeamColor().toString().toLowerCase().substring(1) + "§7.");
                    break;
                }
            }
        }
    }

    public static void teleportPlayersToMap() {
        for (TeamObject teamObject : BridgeFighters.teamObjects) {
            for (Player player : teamObject.getPlayers()) {
                player.teleport(BridgeFighters.getSelectedMap().getTeamSpawnPoints().get(teamObject.getTeamColor()));
            }
        }
    }

    public static void spawnWithers() {
        for (Map.Entry<TeamColor, Location> entry : BridgeFighters.getSelectedMap().getTeamWitherSpawnPoints().entrySet()) {
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                if (teamObject.getTeamColor() == entry.getKey()) {
                    if (teamObject.getPlayers().size() > 0) {
                        World world = ((CraftWorld) entry.getValue().getWorld()).getHandle();
                        CustomWither customWither = new CustomWither(world, entry.getValue(), entry.getKey().getColor() + entry.getKey().toString());
                        world.addEntity(customWither);
                        Data.withers.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        }
    }

    public static void spawnVillagers() {
        for (Location location : BridgeFighters.getSelectedMap().getMiddleVillagers()) {
            World world = ((CraftWorld) location.getWorld()).getHandle();
            CustomVillager customVillager = new CustomVillager(world, location, "§aMittel-Villager");
            world.addEntity(customVillager);
        }
        for (Location location : BridgeFighters.getSelectedMap().getTeamVillagers()) {
            World world = ((CraftWorld) location.getWorld()).getHandle();
            CustomVillager customVillager = new CustomVillager(world, location, "§aTeam-Villager");
            world.addEntity(customVillager);
        }
    }
}
