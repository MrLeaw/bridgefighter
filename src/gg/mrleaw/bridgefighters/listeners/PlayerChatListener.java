//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.ConfigUtils;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.FileManager;
import gg.mrleaw.bridgefighters.utils.SetupState;
import gg.mrleaw.bridgefighters.utils.settings.*;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.io.IOException;

public class PlayerChatListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if (Data.getSetupPlayer() != null && Data.getSetupPlayer() == event.getPlayer()) {
            event.setCancelled(true);
            String msg = event.getMessage();
            if (Data.getSetupState() == SetupState.TEAM_COUNT) {
                int count = Integer.parseInt(msg);
                Teams.setTeam_count(count);
                Data.setSetupState(SetupState.TEAM_SIZE);

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aGib die Größe der Teams ein!");

            } else if (Data.getSetupState() == SetupState.TEAM_SIZE) {
                int size = Integer.parseInt(msg);
                Teams.setTeam_size(size);
                Data.setSetupState(SetupState.LOBBY_SPAWN);

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetze den Lobby-Spawn!");
                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe dazu irgendwas in den Chat ein, sobald du dich auf den Spawnpunkt gestellt hast!");

            } else if (Data.getSetupState() == SetupState.LOBBY_SPAWN) {
                Location loc = event.getPlayer().getLocation();
                BridgeFighters.getGameSettings().setLobby_spawn(loc);
                Data.setSetupState(SetupState.MIDDLE_VILLAGERS);

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetze die Villager der Mitte!");
                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'add' um an der aktuellen Position einen Villager hinzuzufügen");
                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'done' um zum nächsten Schritt zu wechseln!");

            } else if (Data.getSetupState() == SetupState.MIDDLE_VILLAGERS) {
                if (msg.equalsIgnoreCase("add")) {
                    Villagers.getMiddle_villagers().add(event.getPlayer().getLocation());

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aPunkt hinzugefügt!");

                } else if (msg.equalsIgnoreCase("done")) {

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte gib die Tageszeit an (DAY | NIGHT | BOTH)");

                    Data.setSetupState(SetupState.DAYTIME);
                }
            } else if (Data.getSetupState() == SetupState.DAYTIME) {
                Daytime daytime = Daytime.valueOf(msg.toUpperCase());
                BridgeFighters.getGameSettings().setDaytime(daytime);

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte gib die Farben der Teams ein!");

                Data.setSetupCounter(1);

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte gib die Farbe des " + Data.getSetupCounter() + ". Teams an:");

                Data.setSetupState(SetupState.TEAM_COLORS);
            } else if (Data.getSetupState() == SetupState.TEAM_COLORS) {
                TeamColor teamColor = TeamColor.valueOf(msg.toUpperCase());
                Teams.getTeams().add(teamColor);
                int new_counter = Data.getSetupCounter() + 1;
                if (Teams.getTeam_count() <= Data.getSetupCounter()) {
                    Data.setSetupState(SetupState.TEAM_SPAWNPOINTS);

                    TeamColor first = Teams.getTeams().get(0);
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte setze den Spawnpunkt für Team " + first.getColor() + first.toString());
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe dazu irgendwas in den Chat ein, sobald du dich auf den Spawnpunkt gestellt hast!");
                    Data.setSetupCounter(0);

                    return;
                }

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte gib die Farbe des " + new_counter + ". Teams an:");

                Data.setSetupCounter(new_counter);
            } else if (Data.getSetupState() == SetupState.TEAM_SPAWNPOINTS) {
                int new_counter = Data.getSetupCounter();
                Teams.getTeam_spawnpoints().put(Teams.getTeams().get(new_counter), event.getPlayer().getLocation());

                new_counter++;
                if (new_counter >= Teams.getTeam_count()) {
                    Data.setSetupState(SetupState.TEAM_WITHERS);
                    TeamColor first = Teams.getTeams().get(0);
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte setze den Witherpunkt für Team " + first.getColor() + first.toString());
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe dazu irgendwas in den Chat ein, sobald du dich auf den Witherpunkt gestellt hast!");
                    Data.setSetupCounter(0);

                    return;
                }
                TeamColor teamColor = Teams.getTeams().get(new_counter);
                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte setze den Spawnpunkt für Team " + teamColor.getColor() + teamColor.toString());

                Data.setSetupCounter(new_counter);
            } else if (Data.getSetupState() == SetupState.TEAM_WITHERS) {
                int new_counter = Data.getSetupCounter();
                Teams.getTeam_withers().put(Teams.getTeams().get(new_counter), event.getPlayer().getLocation());
                new_counter++;
                if (new_counter >= Teams.getTeam_count()) {
                    Data.setSetupState(SetupState.TEAM_VILLAGERS);

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetze die Villager der Teams!");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'add' um an der aktuellen Position einen Villager hinzuzufügen");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'done' um zum nächsten Schritt zu wechseln!");

                    Data.setSetupCounter(0);
                    return;
                }
                TeamColor teamColor = Teams.getTeams().get(new_counter);

                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aBitte setze den Witherpunkt für Team " + teamColor.getColor() + teamColor.toString());

                Data.setSetupCounter(new_counter);
            } else if (Data.getSetupState() == SetupState.TEAM_VILLAGERS) {
                if (msg.equalsIgnoreCase("add")) {
                    Villagers.getTeam_villagers().add(event.getPlayer().getLocation());

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aPunkt hinzugefügt!");

                } else if (msg.equalsIgnoreCase("done")) {

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetze die " + SpawnerType.BRONZE.getColor() + "Bronzespawner§a!");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'add' um an der aktuellen Position einen Spawner hinzuzufügen");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'done' um zum nächsten Schritt zu wechseln!");

                    Data.setSetupState(SetupState.BRONZE);
                }
            } else if (Data.getSetupState() == SetupState.BRONZE) {
                if (msg.equalsIgnoreCase("add")) {
                    Spawners.getBronze_spawners().add(event.getPlayer().getLocation());

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + SpawnerType.BRONZE.getColor() + "Bronzespawner §ahinzugefügt!");

                } else if (msg.equalsIgnoreCase("done")) {

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetze die " + SpawnerType.IRON.getColor() + "Eisenspawner§a!");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'add' um an der aktuellen Position einen Spawner hinzuzufügen");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'done' um zum nächsten Schritt zu wechseln!");

                    Data.setSetupState(SetupState.IRON);
                }
            } else if (Data.getSetupState() == SetupState.IRON) {
                if (msg.equalsIgnoreCase("add")) {
                    Spawners.getIron_spawners().add(event.getPlayer().getLocation());

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + SpawnerType.IRON.getColor() + "Eisenspawner §ahinzugefügt!");

                } else if (msg.equalsIgnoreCase("done")) {

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetze die " + SpawnerType.GOLD.getColor() + "Goldspawner§a!");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'add' um an der aktuellen Position einen Spawner hinzuzufügen");
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aTippe 'done' um zum nächsten Schritt zu wechseln!");

                    Data.setSetupState(SetupState.GOLD);
                }
            } else if (Data.getSetupState() == SetupState.GOLD) {
                if (msg.equalsIgnoreCase("add")) {
                    Spawners.getGold_spawners().add(event.getPlayer().getLocation());

                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + SpawnerType.GOLD.getColor() + "Goldspawner §ahinzugefügt!");

                } else if (msg.equalsIgnoreCase("done")) {
                    Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aGib den Namen der Map ein!");
                    Data.setSetupState(SetupState.MAPNAME);
                }
            } else if (Data.getSetupState() == SetupState.MAPNAME) {
                BridgeFighters.getGameSettings().setMapname(msg);
                Data.setSetupState(SetupState.BUILDERNAME);
                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aGib den Namen des Builders ein!");
            } else if (Data.getSetupState() == SetupState.BUILDERNAME) {
                BridgeFighters.getGameSettings().setBuildername(msg);
                FileConfiguration cfg = FileManager.getConfigFileConfiguration(BridgeFighters.getGameSettings().getMapname() + ".yml");
                cfg.set("teams.count", Teams.getTeam_count());
                cfg.set("teams.size", Teams.getTeam_size());
                cfg.set("villagers.middle", ConfigUtils.getLocationArrayListString(Villagers.getMiddle_villagers()));
                cfg.set("villagers.teams", ConfigUtils.getLocationArrayListString(Villagers.getTeam_villagers()));
                cfg.set("daytime", BridgeFighters.getGameSettings().getDaytime().toString());
                cfg.set("teams.colors", ConfigUtils.getTeamColorArrayListString(Teams.getTeams()));
                cfg.set("teams.spawnpoints", ConfigUtils.getTeamColorLocationHashMapString(Teams.getTeam_spawnpoints()));
                cfg.set("teams.witherspawnpoints", ConfigUtils.getTeamColorLocationHashMapString(Teams.getTeam_withers()));
                cfg.set("spawners.bronze", ConfigUtils.getLocationArrayListString(Spawners.getBronze_spawners()));
                cfg.set("spawners.iron", ConfigUtils.getLocationArrayListString(Spawners.getIron_spawners()));
                cfg.set("spawners.gold", ConfigUtils.getLocationArrayListString(Spawners.getGold_spawners()));
                cfg.set("lobby.spawn", ConfigUtils.getLocationString(BridgeFighters.getGameSettings().getLobby_spawn()));
                cfg.set("map.name", BridgeFighters.getGameSettings().getMapname());
                cfg.set("map.builder", msg);
                try {
                    cfg.save(FileManager.getConfigFile(BridgeFighters.getGameSettings().getMapname()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Data.getSetupPlayer().sendMessage(Data.getPrefix() + "§aSetup abgeschlossen!");
                Data.setSetupPlayer(null);
            }
        }
        if (!Data.spectators.contains(event.getPlayer())) {
            TeamColor color = null;
            event.getRecipients().clear();
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                if (teamObject.getPlayers().contains(event.getPlayer())) {
                    for (Player player : teamObject.getPlayers()) {
                        event.getRecipients().add(player);
                    }
                    color = teamObject.getTeamColor();
                }
            }
            if (color != null) {
                event.setFormat(color.getColor() + event.getPlayer().getName() + " §8»§7 " + event.getMessage());
            } else {
                event.setFormat(event.getPlayer().getName() + " §8»§7 " + event.getMessage());
            }
        } else {
            event.getRecipients().clear();
            for (Player player : Data.spectators) {
                event.getRecipients().add(player);
            }
            event.setFormat("§7[§c✘§7]" + event.getPlayer().getName() + " §8»§7 " + event.getMessage());
        }
    }

}
