//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (Data.waittomove) {
            if (e.getFrom().getX() != e.getTo().getX() || e.getFrom().getY() != e.getTo().getY() || e.getFrom().getZ() != e.getTo().getZ()) {
                e.setTo(new Location(e.getFrom().getWorld(), e.getFrom().getX(), e.getFrom().getY(), e.getFrom().getZ(), e.getTo().getYaw(), e.getTo().getPitch()));
            }
        }
        if (Data.spectators.contains(e.getPlayer())) {
            if (BridgeFighters.getGameState() == GameState.INGAME) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!Data.spectators.contains(player)) {
                        if (e.getPlayer().getLocation().distance(player.getLocation()) <= 8) {
                            e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().multiply(-1).setY(0.5D));
                        }
                    }
                }
            }
        }
    }

}
