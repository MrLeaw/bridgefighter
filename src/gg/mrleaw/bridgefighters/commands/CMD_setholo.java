//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.commands;

import gg.mrleaw.bridgefighters.utils.ConfigUtils;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.FileManager;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.io.IOException;

public class CMD_setholo implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (command.getName().equalsIgnoreCase("setholo")) {
            if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                if (player.hasPermission("bf.setholo")) {
                    FileConfiguration configuration = FileManager.getWaitingLobbyFileConfiguration();
                    configuration.set("stats_holo", ConfigUtils.getLocationString(((Player) commandSender).getLocation()));
                    try {
                        configuration.save(FileManager.getWaitingLobbyFile());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    commandSender.sendMessage(Data.getPrefix() + "§aErfolgreich!");
                } else {
                    MultiLanguage.sendMessage(((Player) commandSender),
                            Data.getPrefix() + "§cDu hast keine Berechtigungen, diesen Befehl auszuführen!",
                            Data.getPrefix() + "§cYou do not have permissions to execute this command!",
                            Data.getPrefix() + "§cNo tiene permisos para ejecutar este comando!");
                }
            } else {
                commandSender.sendMessage("nihct konsole!");
            }
        }
        return false;
    }
}
