//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.commands;

import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_start implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("start")) {
            boolean hasPermission;
            if (commandSender instanceof Player) {
                hasPermission = ((Player) commandSender).hasPermission("bf.forcestart");
            } else {
                hasPermission = true;
            }
            if (hasPermission) {
                if (Data.lobby_counter_running) if (Data.lobby_counter > 10) Data.lobby_counter = 10;
            } else {
                if (commandSender instanceof Player) {
                    MultiLanguage.sendMessage(((Player) commandSender),
                            Data.getPrefix() + "§cDu hast keine Berechtigungen, diesen Befehl auszuführen!",
                            Data.getPrefix() + "§cYou do not have permissions to execute this command!",
                            Data.getPrefix() + "§cNo tiene permisos para ejecutar este comando!");
                } else {
                    commandSender.sendMessage(Data.getPrefix() + "§cDu hast keine Berechtigungen, diesen Befehl auszuführen!");
                }
            }
        }
        return false;
    }
}
