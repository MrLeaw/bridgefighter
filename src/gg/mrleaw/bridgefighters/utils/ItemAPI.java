//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ItemAPI {

    ItemStack itemStack;
    ItemMeta itemMeta;
    SkullMeta skullMeta;
    Potion potion;

    public ItemAPI(String display, Material material, byte subid, int amount, List<String> lore) {
        itemStack = new ItemStack(material, amount, subid);
        itemMeta = itemStack.getItemMeta();
        if (lore != null) {
            itemMeta.setLore(lore);
        }
        itemMeta.setDisplayName(display);

    }

    public ItemAPI(String display, Material material, byte subid, int amount, ArrayList<String> lore,
                   Enchantment enchantment, int level) {
        itemStack = new ItemStack(material, amount, subid);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(display);
        if (lore != null) {
            itemMeta.setLore(lore);
        }
        itemMeta.addEnchant(enchantment, level, true);
        itemMeta.setDisplayName(display);
    }

    public ItemAPI(String display, Material material, byte subid, int amount, ArrayList<String> lore,
                   Enchantment enchantment) {
        itemStack = new ItemStack(material, amount, subid);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(display);
        if (lore != null) {
            itemMeta.setLore(lore);
        }
        itemMeta.addEnchant(enchantment, 1, true);
        itemMeta.setDisplayName(display);
    }

    public ItemAPI(String display, Material material, byte subid, int amount, ArrayList<String> lore,
                   Enchantment enchantment, int lvl1, Enchantment enchantment2, int lvl2) {
        itemStack = new ItemStack(material, amount, subid);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(display);
        if (lore != null) {
            itemMeta.setLore(lore);
        }
        itemMeta.addEnchant(enchantment, lvl1, true);
        itemMeta.addEnchant(enchantment2, lvl2, true);
        itemMeta.setDisplayName(display);
    }

    public ItemAPI(String display, int amount, ArrayList<String> lore,
                   PotionType potionType, boolean splash) {
        itemStack = new ItemStack(Material.POTION, amount);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(display);
        if (lore != null) {
            itemMeta.setLore(lore);
        }
        potion = new Potion(1);
        potion.setType(potionType);
        potion.setSplash(splash);
    }

    public ItemAPI(String display, String skullOwner, int amount, ArrayList<String> lore) {
        itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        skullMeta = (SkullMeta) itemStack.getItemMeta();
        if (lore != null) {
            skullMeta.setLore(lore);
        }
        skullMeta.setDisplayName(display);
        skullMeta.setOwner(skullOwner);
    }

    public ItemStack build() {
        itemStack.setItemMeta(itemMeta);
        return itemStack;

    }

    public ItemStack buildSkull() {
        itemStack.setItemMeta(skullMeta);
        return itemStack;

    }

    public ItemStack buildPotion() {
        potion.apply(itemStack);
        itemStack.setItemMeta(itemMeta);
        return itemStack;

    }

    public static ItemStack setColorLeatherMeta(ItemStack itemStack, int r, int g, int b) {
        LeatherArmorMeta itemMeta = (LeatherArmorMeta) itemStack.getItemMeta();
        itemMeta.setColor(Color.fromRGB(r, g, b));
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack setLeatherColor(ItemStack itemStack, Color color) {
        LeatherArmorMeta itemMeta = (LeatherArmorMeta) itemStack.getItemMeta();
        itemMeta.setColor(color);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack setColorBannerMeta(ItemStack itemStack, int r, int g, int b) {
        BannerMeta itemMeta = (BannerMeta) itemStack.getItemMeta();
        itemMeta.setBaseColor(DyeColor.getByColor(Color.fromRGB(r, g, b)));
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack setBannerColor(ItemStack itemStack, DyeColor color) {
        BannerMeta itemMeta = (BannerMeta) itemStack.getItemMeta();
        itemMeta.setBaseColor(color);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack createHead(String data, int amount, String name, String lore) {
        ItemStack item = new ItemStack(Material.SKULL_ITEM);
        item.setDurability((short) 3);
        item.setAmount(amount);
        ItemMeta meta = item.getItemMeta();
        if (!lore.equals("")) {
            String[] loreListArray = lore.split("__");
            List<String> loreList = new ArrayList();
            for (String s : loreListArray) {
                loreList.add(s.replace("&", "§"));
            }
            meta.setLore(loreList);
        }
        if (!name.equals("")) {
            meta.setDisplayName(name.replace("&", "§"));
        }
        item.setItemMeta(meta);
        SkullMeta headMeta = (SkullMeta) item.getItemMeta();
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", data));
        try {
            Field profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);
        } catch (Exception localException1) {
        }
        item.setItemMeta(headMeta);
        return item;
    }

}