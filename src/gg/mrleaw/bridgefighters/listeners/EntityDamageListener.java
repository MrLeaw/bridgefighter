//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import gg.mrleaw.bridgefighters.utils.ScoreboardObject;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Map;

public class EntityDamageListener implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Villager) {
            e.setCancelled(true);
        }
        if (e.getEntity() instanceof Wither) {
            if (e.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) {
                e.setCancelled(true);
            }
        }
        if (e.getEntity() instanceof Player) {
            if (e.getCause() == EntityDamageEvent.DamageCause.FALL) {
                if ((((Player) e.getEntity()).getHealth() - e.getDamage()) <= 0) {
                    Data.kill_reasons.put((Player) e.getEntity(), "fall");
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Wither) {
            TeamColor color = null;
            for (Map.Entry<TeamColor, Location> entry : Data.withers.entrySet()) {
                if (entry.getValue().equals(e.getEntity().getLocation())) {
                    color = entry.getKey();
                }
            }
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                if (teamObject.getPlayers().contains(e.getDamager())) {
                    if (teamObject.getTeamColor() == color) {
                        e.setCancelled(true);
                        MultiLanguage.sendMessage((Player) e.getDamager(),
                                Data.getPrefix() + "§cDu darfst deinen Wither nicht angreifen!",
                                Data.getPrefix() + "§cYou can not attack your Wither!",
                                Data.getPrefix() + "No puedes atacar a tu Wither!");
                        return;
                    }
                }
            }
            if (Data.last_wither_hitter.containsKey(color)) {
                Data.last_wither_hitter.replace(color, (Player) e.getDamager());
            } else {
                Data.last_wither_hitter.put(color, (Player) e.getDamager());
            }
            String groupname = color.toString();
            groupname = groupname.substring(0, 1) + groupname.substring(1).toLowerCase();
            groupname = color.getColor() + groupname;
            int new_health = ((int) ((((Wither) e.getEntity()).getHealth() - e.getDamage()) / 3));
            if (new_health < 0) new_health = 0;
            ScoreboardObject.sb.getObjective("aaa").getScore(groupname).setScore(new_health);
            ScoreboardObject.setSideboard();
        } else if (e.getEntity() instanceof Player) {
            if (!Data.spectators.contains(e.getDamager())) {
                if ((((Player) e.getEntity()).getHealth() - e.getDamage()) <= 0) {
                    Data.kill_reasons.put((Player) e.getEntity(), "player." + e.getDamager().getName());
                }
                Player entity = (Player) e.getEntity();
                Player damager = (Player) e.getDamager();
                for (TeamObject teamObject : BridgeFighters.teamObjects) {
                    if (teamObject.getPlayers().contains(entity) && teamObject.getPlayers().contains(damager)) {
                        e.setCancelled(true);
                    }
                }
            }
        }
    }

}
