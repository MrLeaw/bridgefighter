//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.Random;

public class ItemDropManager {

    public static int bronze, iron, gold;

    public static void start() {
        Random r = new Random();
        bronze = Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> {
            for (Location location : BridgeFighters.getSelectedMap().getBronze()) {
                location.getWorld().dropItemNaturally(location, new ItemAPI(r.nextInt(50) + "", Material.CLAY_BRICK, (byte) 0, 1, null).build());
            }
        }, 20, 20);
        iron = Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> {
            for (Location location : BridgeFighters.getSelectedMap().getIron()) {
                location.getWorld().dropItemNaturally(location, new ItemAPI(r.nextInt(50) + "", Material.IRON_INGOT, (byte) 0, 1, null).build());
            }
        }, 20 * 5, 20 * 10);
        gold = Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> {
            for (Location location : BridgeFighters.getSelectedMap().getGold()) {
                location.getWorld().dropItemNaturally(location, new ItemAPI(r.nextInt(50) + "", Material.GOLD_INGOT, (byte) 0, 1, null).build());
            }
        }, 20 * 15, 20 * 30);
    }

}
