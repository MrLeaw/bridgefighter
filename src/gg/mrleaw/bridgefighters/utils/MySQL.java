//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    public static String host, username, password, database, port;
    private static Connection con;

    public static void connect() {
        if (!isConnected()) {
            try {
                con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§aBridgeFighters-MySQL-Connection ready!");
            } catch (SQLException e) {
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cBridgeFighters-MySQL-Connection failed! Make sure the data you entered in the config.yml is correct and your MySQL Server is accessible.");
            }
        }
    }

    public static void close() {
        if (isConnected()) {
            try {
                con.close();
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§aBridgeFighters-MySQL-Connection closed!");
            } catch (SQLException e) {
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cUnable to close BridgeFighters-MySQL-Connection!");
            }
        }
    }

    public static boolean isConnected() {
        return con != null;
    }

    public static void createTable() {
        if (isConnected()) {
            try {
                con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS `stats` (`uuid` VARCHAR(46) NOT NULL,`name` VARCHAR(16) NOT NULL, `kills` BIGINT NOT NULL, `deaths` BIGINT NOT NULL, `points` BIGINT NOT NULL, `wongames` BIGINT NOT NULL, `games` BIGINT NOT NULL ,PRIMARY KEY (`uuid`)) ENGINE = InnoDB;");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void update(String qry) {
        if (isConnected()) {
            try {
                con.createStatement().executeUpdate(qry);
            } catch (SQLException e) {
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cUnable to access BridgeFighters MySQL database.");
            }
        }
    }

    public static ResultSet getResult(String qry) {
        if (isConnected()) {
            try {
                ResultSet rs = con.createStatement().executeQuery(qry);
                return rs;
            } catch (SQLException e) {
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cUnable to access BridgeFighters MySQL database.");
            }
        }else {
            Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cUnable to access BridgeFighters MySQL database.");
        }
        return null;
    }

    public static void disableAutoDisconnect() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> getResult("SELECT * FROM `stats`"), 20 * 60 * 5, 20 * 60 * 5);
    }

}
