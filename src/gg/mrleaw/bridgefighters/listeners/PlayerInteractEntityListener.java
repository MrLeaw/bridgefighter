//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;

public class PlayerInteractEntityListener implements Listener {

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        if (Data.spectators.contains(e.getPlayer())) {
            e.setCancelled(true);
        } else {
            if (e.getRightClicked() instanceof Villager) {
                if (e.getRightClicked().getName().equals("§aTeam-Villager")) {
                    e.setCancelled(true);
                    Player player = e.getPlayer();
                    Inventory inventory = Bukkit.createInventory(e.getPlayer(), 4 * 9, "§bShop");
                    inventory.setItem(0, new ItemAPI("Blocks", Material.SANDSTONE, (byte) 0, 1, null).build());
                    inventory.setItem(1, new ItemAPI("Armor", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build());
                    inventory.setItem(2, new ItemAPI("Tools", Material.IRON_PICKAXE, (byte) 0, 1, null).build());
                    inventory.setItem(3, new ItemAPI("Swords", Material.GOLD_SWORD, (byte) 0, 1, null).build());
                    inventory.setItem(4, new ItemAPI("Food", Material.APPLE, (byte) 0, 1, null).build());
                    inventory.setItem(5, new ItemAPI("Chests", Material.CHEST, (byte) 0, 1, null).build());
                    inventory.setItem(6, new ItemAPI("Potions", Material.POTION, (byte) 0, 1, null).build());
                    inventory.setItem(7, new ItemAPI("Specials", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build());
                    for (int i = 9; i < 18; i++) {
                        inventory.setItem(i, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 7, 1, null).build());
                    }
                    inventory.setItem(9, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.SANDSTONE, (byte) 0, 2, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.ENDER_STONE, (byte) 0, 1, null).build());
                    inventory.setItem(20, new ItemAPI("", Material.IRON_BLOCK, (byte) 0, 1, null).build());
                    inventory.setItem(21, new ItemAPI("", Material.ICE, (byte) 0, 1, null).build());
                    inventory.setItem(22, new ItemAPI("", Material.WATER_LILY, (byte) 0, 1, null).build());
                    inventory.setItem(23, new ItemAPI("", Material.SANDSTONE_STAIRS, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 1, null).build());
                    inventory.setItem(28, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 2, null).build());
                    inventory.setItem(30, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 4, null).build());
                    inventory.setItem(31, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                    inventory.setItem(32, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 3, null).build());
                    player.openInventory(inventory);
                } else if (e.getRightClicked().getName().equals("§aMittel-Villager")) {
                    e.setCancelled(true);
                    Player player = e.getPlayer();
                    Inventory inventory = Bukkit.createInventory(e.getPlayer(), 4 * 9, "§bMittel-Shop");
                    inventory.setItem(0, new ItemAPI("Blocks", Material.SANDSTONE, (byte) 0, 1, null).build());
                    inventory.setItem(1, new ItemAPI("Armor", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build());
                    inventory.setItem(2, new ItemAPI("Tools", Material.IRON_PICKAXE, (byte) 0, 1, null).build());
                    inventory.setItem(3, new ItemAPI("Swords", Material.GOLD_SWORD, (byte) 0, 1, null).build());
                    inventory.setItem(4, new ItemAPI("Bows", Material.BOW, (byte) 0, 1, null).build());
                    inventory.setItem(5, new ItemAPI("Food", Material.APPLE, (byte) 0, 1, null).build());
                    inventory.setItem(6, new ItemAPI("Chests", Material.CHEST, (byte) 0, 1, null).build());
                    inventory.setItem(7, new ItemAPI("Potions", Material.POTION, (byte) 0, 1, null).build());
                    inventory.setItem(8, new ItemAPI("Specials", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build());
                    for (int i = 9; i < 18; i++) {
                        inventory.setItem(i, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 7, 1, null).build());
                    }
                    inventory.setItem(9, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.SANDSTONE, (byte) 0, 2, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.ENDER_STONE, (byte) 0, 1, null).build());
                    inventory.setItem(20, new ItemAPI("", Material.IRON_BLOCK, (byte) 0, 1, null).build());
                    inventory.setItem(21, new ItemAPI("", Material.ICE, (byte) 0, 1, null).build());
                    inventory.setItem(22, new ItemAPI("", Material.WATER_LILY, (byte) 0, 1, null).build());
                    inventory.setItem(23, new ItemAPI("", Material.SANDSTONE_STAIRS, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 1, null).build());
                    inventory.setItem(28, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 2, null).build());
                    inventory.setItem(30, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 4, null).build());
                    inventory.setItem(31, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                    inventory.setItem(32, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 3, null).build());
                    player.openInventory(inventory);
                }
            }
        }
    }

}