//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MapSettings;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ForcemapListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getClickedInventory() != null && e.getClickedInventory().getName() != null && e.getClickedInventory().getName().equals("§bForcemap")) {
            e.setCancelled(true);
            if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.MAP) {
                System.out.println(e.getCurrentItem().getItemMeta().getDisplayName());
                e.getWhoClicked().closeInventory();
                MultiLanguage.sendMessage((Player) e.getWhoClicked(),
                        Data.getPrefix() + "§aDu hast die Map '§6" + e.getCurrentItem().getItemMeta().getDisplayName() + "§a' gewählt!",
                        Data.getPrefix() + "§aYou have chosen the map '§6" + e.getCurrentItem().getItemMeta().getDisplayName() + "§a'!",
                        Data.getPrefix() + "§a¡Has elegido el mapa '§6" + e.getCurrentItem().getItemMeta().getDisplayName() + "§a'!");
                for (MapSettings mapSettings : BridgeFighters.getMaps()) {
                    if (mapSettings.getMapname().equals(e.getCurrentItem().getItemMeta().getDisplayName())) {
                        BridgeFighters.setSelectedMap(mapSettings);
                    }
                }
            }
        }
    }

}
