//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.commands;

import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.StatsManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class CMD_stats implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (args.length == 0) {
            if (commandSender instanceof Player) {
                UUID uuid = ((Player) commandSender).getUniqueId();
                commandSender.sendMessage(Data.getPrefix() + "§aDeine Stats");
                commandSender.sendMessage(Data.getPrefix() + "Rang: §6" + StatsManager.getRang(uuid));
                commandSender.sendMessage(Data.getPrefix() + "Kills: §6" + StatsManager.getKills(uuid));
                commandSender.sendMessage(Data.getPrefix() + "Deaths: §6" + StatsManager.getDeaths(uuid));
                commandSender.sendMessage(Data.getPrefix() + "K/D: §6" + StatsManager.getKD(uuid));
                commandSender.sendMessage(Data.getPrefix() + "Points: §6" + StatsManager.getPoints(uuid));
                commandSender.sendMessage(Data.getPrefix() + "Played Games: §6" + StatsManager.getGames(uuid));
                commandSender.sendMessage(Data.getPrefix() + "Won Games: §6" + StatsManager.getWonGames(uuid));
                commandSender.sendMessage(Data.getPrefix() + "Lost Games: §6" + StatsManager.getLostGames(uuid));


            } else {
                commandSender.sendMessage(Data.getPrefix() + "§cDu musst ein Spieler sein oder wenigstens einen angeben!");
            }
        } else if (args.length == 1) {
            if (commandSender instanceof Player) {
                Player player = ((Player) commandSender);
                if (player.hasPermission("bf.seeothersstats")) {
                    commandSender.sendMessage(Data.getPrefix() + "§cDu hast nicht genügend Rechte!");
                    return false;
                }
            }
            UUID uuid = Bukkit.getOfflinePlayer(args[0]).getUniqueId();
            commandSender.sendMessage(Data.getPrefix() + "§a" + args[0] + "'s Stats");
            commandSender.sendMessage(Data.getPrefix() + "Rang: §6" + StatsManager.getRang(uuid));
            commandSender.sendMessage(Data.getPrefix() + "Kills: §6" + StatsManager.getKills(uuid));
            commandSender.sendMessage(Data.getPrefix() + "Deaths: §6" + StatsManager.getDeaths(uuid));
            commandSender.sendMessage(Data.getPrefix() + "K/D: §6" + StatsManager.getKD(uuid));
            commandSender.sendMessage(Data.getPrefix() + "Points: §6" + StatsManager.getPoints(uuid));
            commandSender.sendMessage(Data.getPrefix() + "Won Games: §6" + StatsManager.getWonGames(uuid));
            commandSender.sendMessage(Data.getPrefix() + "Lost Games: §6" + StatsManager.getLostGames(uuid));
        }
        return false;
    }
}
