//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.GameState;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class JumpNRunListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (Data.jumpNRunPlayers.contains(e.getPlayer())) {
            if (e.getTo().getBlock().getType() == Material.WATER || e.getTo().getBlock().getType() == Material.STATIONARY_WATER) {
                e.getPlayer().teleport(Data.jumpNRunLocation);
            }
        }
    }

    @EventHandler
    public void onLeaveJumpNRunMode(PlayerInteractEvent e) {
        if (BridgeFighters.getGameState() == GameState.LOBBY) {
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK
                    || e.getAction() == Action.RIGHT_CLICK_AIR
                    || e.getAction() == Action.LEFT_CLICK_AIR
                    || e.getAction() == Action.LEFT_CLICK_BLOCK) {
                if (e.getItem() != null && e.getItem().getType() == Material.SLIME_BALL) {
                    e.setCancelled(true);
                    Data.jumpNRunPlayers.remove(e.getPlayer());
                    e.getPlayer().teleport(BridgeFighters.getGameSettings().getLobby_spawn());
                    e.getPlayer().getInventory().setItem(4, new ItemStack(Material.AIR));
                    MultiLanguage.sendMessage(e.getPlayer(),
                            Data.getPrefix() + "§cJumpNRun-Modus verlassen!",
                            Data.getPrefix() + "§cLeft JumpNRun-Mode!",
                            Data.getPrefix() + "§cModo JumpNRun izquierdo!");
                }
            }
        }
    }

}
