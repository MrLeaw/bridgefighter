//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.commands;

import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.Language;
import gg.mrleaw.bridgefighters.utils.SetupState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_setup implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (command.getName().equalsIgnoreCase("setup")) {
            if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                if (player.hasPermission("bf.setup")) {
                    Data.setSetupPlayer(player);
                    player.sendMessage("");
                    player.sendMessage(Data.getPrefix() + "§aSetup-Modus gestartet!");
                    player.sendMessage("");
                    Data.setSetupState(SetupState.TEAM_COUNT);
                    player.sendMessage("");
                    player.sendMessage(Data.getPrefix() + "§aGib die Anzahl der Teams ein!");
                    player.sendMessage("");
                } else {
                    player.sendMessage("");
                    if (Data.language == Language.EN) {
                        player.sendMessage(Data.getPrefix() + "§cYou have no permissions to perform this command!");
                    } else if (Data.language == Language.DE) {
                        player.sendMessage(Data.getPrefix() + "§cDu hast keine Berechtigungen, diesen Befehl auszuführen!");
                    }
                    player.sendMessage("");
                }
            }
        }
        return false;
    }
}
