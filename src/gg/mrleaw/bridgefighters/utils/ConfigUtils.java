//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ConfigUtils {

    // by MrLeaw

    static int getInt(String string) {
        return Integer.parseInt(string);
    }

    public static double getDouble(String string) {
        return Double.parseDouble(string);
    }

    public static float getFloat(String string) {
        return Float.parseFloat(string);
    }

    public static long getLong(String string) {
        return Long.parseLong(string);
    }

    static ArrayList<TeamColor> getTeamColorArrayList(String string) {
        String[] strings = string.split(",");
        ArrayList<TeamColor> teamColors = new ArrayList<>();
        for (String s : strings) {
            teamColors.add(TeamColor.valueOf(s.toUpperCase()));
        }
        return teamColors;
    }

    public static String getTeamColorArrayListString(ArrayList<TeamColor> arrayList) {
        StringBuilder result = new StringBuilder();
        for (TeamColor teamColor : arrayList) {
            result.append(teamColor.toString()).append(",");
        }
        return result.substring(0, result.length() - 1);
    }

    public static String getLocationArrayListString(ArrayList<Location> arrayList) {
        StringBuilder result = new StringBuilder();
        for (Location location : arrayList) {
            result.append(location.getWorld().getName()).append(";").append(location.getX()).append(";").append(location.getY()).append(";").append(location.getZ()).append(";").append(location.getYaw()).append(";").append(location.getPitch()).append(",");
        }
        return result.substring(0, result.length() - 1);
    }

    public static String getTeamColorLocationHashMapString(HashMap<TeamColor, Location> hashMap) {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<TeamColor, Location> entry : hashMap.entrySet()) {
            result.append(entry.getKey().toString()).append(":").append(entry.getValue().getWorld().getName()).append(";").append(entry.getValue().getX()).append(";").append(entry.getValue().getY()).append(";").append(entry.getValue().getZ()).append(";").append(entry.getValue().getYaw()).append(";").append(entry.getValue().getPitch()).append(",");
        }
        return result.substring(0, result.length() - 1);
    }

    public static String getIntLocationMapString(Map<Integer, Location> hashMap) {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<Integer, Location> entry : hashMap.entrySet()) {
            result.append(entry.getKey().toString()).append(":").append(entry.getValue().getWorld().getName()).append(";").append(entry.getValue().getX()).append(";").append(entry.getValue().getY()).append(";").append(entry.getValue().getZ()).append(";").append(entry.getValue().getYaw()).append(";").append(entry.getValue().getPitch()).append(",");
        }
        return result.substring(0, result.length() - 1);
    }

    public static HashMap<TeamColor, Location> getTeamColorLocationHashMap(String string) {
        HashMap<TeamColor, Location> hashMap = new HashMap<>();
        String[] colors = string.split(",");
        for (String color : colors) {
            String[] colorsplit = color.split(":");
            String[] locationsplit = colorsplit[1].split(";");
            TeamColor teamColor = TeamColor.valueOf(colorsplit[0].toUpperCase());
            boolean is_registered = false;
            for (World world : Bukkit.getWorlds()) {
                if (world.getName().equalsIgnoreCase(locationsplit[0])) {
                    is_registered = true;
                }
            }
            if (!is_registered) {
                Bukkit.createWorld(new WorldCreator(locationsplit[0]));
            }
            Location location = new Location(Bukkit.getWorld(locationsplit[0]), Double.parseDouble(locationsplit[1]), Double.parseDouble(locationsplit[2]), Double.parseDouble(locationsplit[3]), Float.parseFloat(locationsplit[4]), Float.parseFloat(locationsplit[5]));
            hashMap.put(teamColor, location);
        }
        return hashMap;
    }

    public static Map<Integer, Location> getIntLocationMap(String string) {
        Map<Integer, Location> hashMap = new TreeMap<>();
        String[] signs = string.split(",");
        for (String sign : signs) {
            String[] signsplit = sign.split(":");
            String[] locationsplit = signsplit[1].split(";");
            Integer integer = Integer.parseInt(signsplit[0]);
            boolean is_registered = false;
            for (World world : Bukkit.getWorlds()) {
                if (world.getName().equalsIgnoreCase(locationsplit[0])) {
                    is_registered = true;
                }
            }
            if (!is_registered) {
                Bukkit.createWorld(new WorldCreator(locationsplit[0]));
            }
            Location location = new Location(Bukkit.getWorld(locationsplit[0]), Double.parseDouble(locationsplit[1]), Double.parseDouble(locationsplit[2]), Double.parseDouble(locationsplit[3]), Float.parseFloat(locationsplit[4]), Float.parseFloat(locationsplit[5]));
            hashMap.put(integer, location);
        }
        return hashMap;
    }

    public static ArrayList<Location> getLocationArrayList(String string) {
        String[] strings = string.split(",");
        ArrayList<Location> locations = new ArrayList<>();
        for (String s : strings) {
            String[] locationsplit = s.split(";");
            Location location = new Location(Bukkit.getWorld(locationsplit[0]), Double.parseDouble(locationsplit[1]), Double.parseDouble(locationsplit[2]), Double.parseDouble(locationsplit[3]), Float.parseFloat(locationsplit[4]), Float.parseFloat(locationsplit[5]));
            locations.add(location);
        }
        return locations;
    }

    public static String getLocationString(Location location) {
        return location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ() + ";" + location.getYaw() + ";" + location.getPitch();
    }

    public static Location getLocation(String string) {
        String[] split = string.split(";");
        return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));
    }

    public static String getWorldName(String string) {
        String[] split = string.split(";");
        return split[0];
    }

    public static String getWorldNameFromHashMap(String string) {
        HashMap<TeamColor, Location> hashMap = getTeamColorLocationHashMap(string);
        String world = "";
        for (Map.Entry<TeamColor, Location> entry : hashMap.entrySet()) {
            world = entry.getValue().getWorld().getName();
        }
        return world;
    }
}
