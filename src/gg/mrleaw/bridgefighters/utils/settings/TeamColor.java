//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils.settings;

public enum TeamColor {

    LIGHT_BLUE("§9", (byte) 3),
    LIGHT_RED("§c", (byte) 6),
    GOLD("§6", (byte) 1),
    LIGHT_GREEN("§a", (byte) 5),
    WHITE("§f", (byte) 0),
    PINK("§d", (byte) 2),
    YELLOW("§e", (byte) 4),
    PURPLE("§5", (byte) 10),
    GRAY("§8", (byte) 7),
    LIGHT_GRAY("§7", (byte) 8),
    LIGHT_CYAN("§b", (byte) 9),
    CYAN("§3", (byte) 9),
    BLUE("§1", (byte) 11),
    BLACK("§0", (byte) 15),
    GREEN("§2", (byte) 13),
    RED("§4", (byte) 14);

    private String color;
    private byte data;

    TeamColor(String color, byte data) {
        this.color = color;
        this.data = data;
    }

    public String getColor() {
        return color;
    }

    public byte getData() {
        return data;
    }
}
