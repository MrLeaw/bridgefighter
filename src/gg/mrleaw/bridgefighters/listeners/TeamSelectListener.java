//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.GameState;
import gg.mrleaw.bridgefighters.utils.ItemAPI;
import gg.mrleaw.bridgefighters.utils.Language;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import gg.mrleaw.bridgefighters.utils.settings.Teams;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;

public class TeamSelectListener implements Listener {

    @EventHandler
    public void onTeamSelect(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (BridgeFighters.getGameState() == GameState.LOBBY) {
                if (event.getItem() != null && event.getItem().getType() == Material.BOOK) {
                    event.setCancelled(true);
                    Player player = event.getPlayer();
                    double result = Teams.getTeam_count() / 4D;
                    int rows = (int) Math.ceil(result);
                    Inventory inventory = Bukkit.createInventory(player, rows * 9, "§bSelect team...");
                    if (Data.language == Language.DE) {
                        inventory = Bukkit.createInventory(player, rows * 9, "§bTeam wählen...");
                    } else if (Data.language == Language.EN) {
                        inventory = Bukkit.createInventory(player, rows * 9, "§bSelect team...");
                    } else if (Data.language == Language.ES) {
                        inventory = Bukkit.createInventory(player, rows * 9, "§bSelecciona un equipo...");
                    }
                    int counter = 1;
                    for (TeamColor teamColor : Teams.getTeams()) {
                        ArrayList<String> lore = new ArrayList<>();
                        for (int i = 0; i < Teams.getTeam_size(); i++) {
                            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                                if (teamObject.getTeamColor() == teamColor) {
                                    if (teamObject.getPlayers().size() > i) {
                                        lore.add("§7" + teamObject.getPlayers().get(i).getDisplayName());
                                    } else {
                                        lore.add("§8---");
                                    }
                                }
                            }
                        }
                        inventory.setItem(counter, new ItemAPI(teamColor.getColor() + teamColor.toString(), Material.STAINED_CLAY, teamColor.getData(), 1, lore).build());
                        counter += 2;
                        switch (counter) {
                            case 9:
                            case 18:
                            case 27: {
                                counter++;
                            }
                        }
                    }
                    player.openInventory(inventory);
                }
            } else if (BridgeFighters.getGameState() == GameState.INGAME) {
                if (event.getItem() != null && event.getItem().getType() == Material.COMPASS) {
                    event.setCancelled(true);
                    Player player = event.getPlayer();
                    double result = Teams.getTeam_count() / 9D;
                    int rows = (int) Math.ceil(result);
                    Inventory inventory = Bukkit.createInventory(player, rows * 9, "§bNavigator");
                    if (Data.language == Language.DE){
                        inventory = Bukkit.createInventory(player, rows * 9, "§bNavigator");
                    } else if (Data.language == Language.EN) {
                        inventory = Bukkit.createInventory(player, rows * 9, "§bNavigator");
                    } else if (Data.language == Language.ES) {
                        inventory = Bukkit.createInventory(player, rows * 9, "§bNavegador");
                    }
                    int counter = 0;
                    for (TeamObject teamObject : BridgeFighters.teamObjects) {
                        for (Player player1 : teamObject.getPlayers()) {
                            inventory.setItem(counter, new ItemAPI(teamObject.getTeamColor().getColor() + player1.getDisplayName(), Material.SKULL_ITEM, (byte) 0, 1, null).build());
                            counter++;
                        }
                    }
                    player.openInventory(inventory);
                }
            }
        }
    }

}
