//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.utils.Data;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerPickupItemListener implements Listener {

    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent e) {
        if (Data.spectators.contains(e.getPlayer())) {
            e.setCancelled(true);
        } else {
            if (e.getItem().getItemStack().getType().equals(Material.CLAY_BRICK) || e.getItem().getItemStack().getType().equals(Material.IRON_INGOT) || e.getItem().getItemStack().getType().equals(Material.GOLD_INGOT)) {
                if (!(e.getItem().getItemStack().getItemMeta().getDisplayName().equals("§cBronze") || e.getItem().getItemStack().getItemMeta().getDisplayName().equals("§7Iron") || e.getItem().getItemStack().getItemMeta().getDisplayName().equals("§6Gold"))) {

                    ItemStack itemStack;
                    ItemMeta itemMeta;

                    itemStack = e.getItem().getItemStack();
                    itemMeta = itemStack.getItemMeta();
                    if (e.getItem().getItemStack().getType().equals(Material.CLAY_BRICK)) {
                        itemMeta.setDisplayName("§cBronze");
                    } else if (e.getItem().getItemStack().getType().equals(Material.IRON_INGOT)) {
                        itemMeta.setDisplayName("§7Iron");
                    } else if (e.getItem().getItemStack().getType().equals(Material.GOLD_INGOT)) {
                        itemMeta.setDisplayName("§6Gold");
                    }
                    itemStack.setItemMeta(itemMeta);
                    e.getPlayer().getInventory().remove(e.getItem().getItemStack());
                    e.getPlayer().getInventory().addItem(itemStack);
                }
            }
        }
    }

}
