//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils.settings;

import org.bukkit.Location;

import java.util.ArrayList;

public class Spawners {

    private static ArrayList<Location> bronze_spawners = new ArrayList<>();
    private static ArrayList<Location> iron_spawners = new ArrayList<>();
    private static ArrayList<Location> gold_spawners = new ArrayList<>();

    public static ArrayList<Location> getBronze_spawners() {
        return bronze_spawners;
    }

    public static void setBronze_spawners(ArrayList<Location> bronze_spawners) {
        Spawners.bronze_spawners = bronze_spawners;
    }

    public static ArrayList<Location> getIron_spawners() {
        return iron_spawners;
    }

    public static void setIron_spawners(ArrayList<Location> iron_spawners) {
        Spawners.iron_spawners = iron_spawners;
    }

    public static ArrayList<Location> getGold_spawners() {
        return gold_spawners;
    }

    public static void setGold_spawners(ArrayList<Location> gold_spawners) {
        Spawners.gold_spawners = gold_spawners;
    }
}
