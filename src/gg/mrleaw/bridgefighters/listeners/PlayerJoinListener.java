//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.VisibilityManager;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.*;
import gg.mrleaw.bridgefighters.utils.settings.Daytime;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        StatsManager.updateOfflineName(p.getUniqueId(), p.getName());
        if (BridgeFighters.getGameState() == GameState.LOBBY) {
            spawnSigns();
            spawnHeads();
            spawnHolo(p);
            ScoreboardObject.setPrefix(p);
            e.setJoinMessage(Data.getPrefix() + "§7+ " + p.getDisplayName());
            Bukkit.createWorld(new WorldCreator(BridgeFighters.getGameSettings().getLobby_spawn().getWorld().getName()));
            p.teleport(BridgeFighters.getGameSettings().getLobby_spawn());
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getWorld() != BridgeFighters.getGameSettings().getLobby_spawn().getWorld()) {
                    player.teleport(BridgeFighters.getGameSettings().getLobby_spawn());
                }
            }
            setDefaultStuff(p);
            Data.gamePlayers.add(p);
            if (Bukkit.getOnlinePlayers().size() >= Data.lobby_min_to_start) {
                if (!Data.lobby_counter_running) {
                    Data.lobby_counter_running = true;
                    MultiLanguage.broadcastMessage(
                            Data.getPrefix() + "§aCountdown gestartet!",
                            Data.getPrefix() + "§aCountdown started!",
                            Data.getPrefix() + "§aCuenta atrás comenzó!");
                    Data.lobby_counter_task = Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> {
                        switch (Data.lobby_counter) {
                            case 60:
                            case 50:
                            case 40:
                            case 30:
                            case 20:
                            case 5:
                            case 4:
                            case 3:
                            case 2: {
                                MultiLanguage.broadcastMessage(
                                        Data.getPrefix() + "§7Das Spiel startet in §6" + Data.lobby_counter + "§7 Sekunden!",
                                        Data.getPrefix() + "§7Game starts in §6" + Data.lobby_counter + "§7 seconds!",
                                        Data.getPrefix() + "§7El juego comienza en §6" + Data.lobby_counter + "§7 segundos!");
                                Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_PLING, 10, 1));
                                break;
                            }
                            case 0: {
                                ArrayList<Player> noteam = new ArrayList<>(Bukkit.getOnlinePlayers());
                                for (TeamObject teamObject : BridgeFighters.teamObjects) {
                                    for (Player player : teamObject.getPlayers()) {
                                        noteam.remove(player);
                                    }
                                }
                                if (noteam.size() == 0) {
                                    int allplayers = Bukkit.getOnlinePlayers().size();
                                    for (TeamObject teamObject : BridgeFighters.teamObjects) {
                                        if (teamObject.getPlayers().size() == allplayers) {
                                            Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.ARROW_HIT, 10, 1));
                                            Data.lobby_counter = 60;
                                            MultiLanguage.broadcastMessage(Data.getPrefix() + "§cDa alle Spieler im selben Team sind, kann das Spiel nicht starten.",
                                                    Data.getPrefix() + "§cSince all players are in the same team, the game can not start.",
                                                    Data.getPrefix() + "§cDado que todos los jugadores están en el mismo equipo, el juego no puede comenzar.");
                                            break;
                                        }
                                    }
                                }
                                MultiLanguage.broadcastMessage(
                                        Data.getPrefix() + "§7Das Spiel startet in §6" + Data.lobby_counter + "§7 Sekunden!",
                                        Data.getPrefix() + "§7Game starts in §6" + Data.lobby_counter + "§7 seconds!",
                                        Data.getPrefix() + "§7El juego comienza en §6" + Data.lobby_counter + "§7 segundos!");
                                for (TeamObject teamObject : BridgeFighters.teamObjects) {
                                    for (TeamObject teamObject1 : BridgeFighters.allPlayers) {
                                        if (teamObject.getTeamColor() == teamObject1.getTeamColor()) {
                                            teamObject1.setPlayers(teamObject.getPlayers());
                                        }
                                    }
                                }
                                Data.jumpNRunPlayers.clear();
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    player.getInventory().clear();
                                }
                                break;
                            }
                            case 10: {
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    player.closeInventory();
                                }
                                if (BridgeFighters.getSelectedMap() == null) {
                                    Random randomGenerator = new Random();
                                    int index = randomGenerator.nextInt(BridgeFighters.getMaps().size());
                                    MapSettings it = BridgeFighters.getMaps().get(index);
                                    BridgeFighters.setSelectedMap(it);
                                }
                                if(BridgeFighters.getSelectedMap().getDaytime() == Daytime.DAY) {
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");
                                    BridgeFighters.getSelectedMap().getMiddleVillagers().get(0).getWorld().setTime(3000);
                                }else if(BridgeFighters.getSelectedMap().getDaytime() == Daytime.NIGHT) {
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle false");
                                    BridgeFighters.getSelectedMap().getMiddleVillagers().get(0).getWorld().setTime(15000);
                                }else {
                                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule doDaylightCycle true");
                                    BridgeFighters.getSelectedMap().getMiddleVillagers().get(0).getWorld().setTime(new Random().nextInt(18000));
                                }
                                MultiLanguage.broadcastMessage(
                                        Data.getPrefix() + "§7Das Spiel startet in §6" + Data.lobby_counter + "§7 Sekunden!",
                                        Data.getPrefix() + "§7Game starts in §6" + Data.lobby_counter + "§7 seconds!",
                                        Data.getPrefix() + "§7El juego comienza en §6" + Data.lobby_counter + "§7 segundos!");
                                MultiLanguage.broadcastTitle(
                                        "§b" + BridgeFighters.getSelectedMap().getMapname(), "§7gebaut von §6" + BridgeFighters.getSelectedMap().getBuildername(),
                                        "§b" + BridgeFighters.getSelectedMap().getMapname(), "§7build by §6" + BridgeFighters.getSelectedMap().getBuildername(),
                                        "§b" + BridgeFighters.getSelectedMap().getMapname(), "§7construir por §6" + BridgeFighters.getSelectedMap().getBuildername(), 90);
                                Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_PLING, 10, 1));
                                Bukkit.getOnlinePlayers().forEach(player -> player.getInventory().clear());
                                break;
                            }
                            case 1: {
                                MultiLanguage.broadcastMessage(
                                        Data.getPrefix() + "§7Das Spiel startet in §6" + Data.lobby_counter + "§7 Sekunde!",
                                        Data.getPrefix() + "§7Game starts in §6" + Data.lobby_counter + "§7 second!",
                                        Data.getPrefix() + "§7El juego comienza en §6" + Data.lobby_counter + "§7 segundos!");
                                Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_PLING, 10, 1));
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            player.setLevel(Data.lobby_counter);
                        }
                        if (!(Bukkit.getOnlinePlayers().size() >= Data.lobby_min_to_start)) {
                            Data.lobby_counter = 61;
                            Data.lobby_counter_running = false;
                            Bukkit.getScheduler().cancelTask(Data.lobby_counter_task);
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                player.setLevel(0);
                            }
                            MultiLanguage.broadcastMessage(
                                    Data.getPrefix() + "§cDer Spielstart wurde abgebrochen!",
                                    Data.getPrefix() + "§cThe game start was canceled!",
                                    Data.getPrefix() + "§cEl inicio del juego fue cancelado!");
                            BridgeFighters.setSelectedMap(null);
                        }
                        if (Data.lobby_counter > 0) {
                            Data.lobby_counter--;
                        } else {
                            Data.lobby_counter = 60;
                            Data.lobby_counter_running = false;
                            BridgeFighters.setGameState(GameState.INGAME);
                            Bukkit.getScheduler().cancelTask(Data.lobby_counter_task);
                            GameStartedManager.setTeams();
                            GameStartedManager.teleportPlayersToMap();
                            GameStartedManager.spawnVillagers();
                            ScoreboardObject.registerSideboard();
                            ScoreboardObject.setSideboard();
                            GameStartedManager.spawnWithers();
                            Data.waittomove = true;
                            MultiLanguage.broadcastMessage(
                                    Data.getPrefix() + "§7Du kannst dich in §6" + Data.waittomove_counter + "§7 Sekunden bewegen!",
                                    Data.getPrefix() + "§7You can move in §6" + Data.waittomove_counter + "§7 seconds!",
                                    Data.getPrefix() + "§7¡Puedes moverte en §6" + Data.waittomove_counter + "§7 segundos!");
                            MultiLanguage.broadcastTitle(
                                    "§43",
                                    "",
                                    "§43",
                                    "",
                                    "§43",
                                    "", 20);
                            MultiLanguage.broadcastSound(Sound.NOTE_PLING);
                            for(TeamObject teamObject : BridgeFighters.allPlayers) {
                                for(Player player : teamObject.getPlayers()) {
                                    player.setGameMode(GameMode.SURVIVAL);
                                }
                            }
                            Data.waittomove_task = Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> {
                                Data.waittomove_counter--;
                                if (Data.waittomove_counter == 0) {
                                    Data.waittomove = false;
                                    Bukkit.getScheduler().cancelTask(Data.waittomove_task);
                                    MultiLanguage.broadcastMessage(
                                            Data.getPrefix() + "§7Du kannst dich §6nun §7bewegen!",
                                            Data.getPrefix() + "§7You can move §6now§7!",
                                            Data.getPrefix() + "§7¡Puedes moverte §6ahora§7!");
                                    MultiLanguage.broadcastTitle(
                                            "§aGO",
                                            "",
                                            "§aGO",
                                            "",
                                            "§aGO",
                                            "", 20);
                                    MultiLanguage.broadcastSound(Sound.LEVEL_UP);
                                } else {
                                    MultiLanguage.broadcastMessage(
                                            Data.getPrefix() + "§7Du kannst dich in §6" + Data.waittomove_counter + "§7 Sekunden bewegen!",
                                            Data.getPrefix() + "§7You can move in §6" + Data.waittomove_counter + "§7 seconds!",
                                            Data.getPrefix() + "§7¡Puedes moverte en §6" + Data.waittomove_counter + "§7 segundos!");
                                    if (Data.waittomove_counter == 2) {
                                        MultiLanguage.broadcastTitle(
                                                "§c2",
                                                "",
                                                "§c2",
                                                "",
                                                "§c2",
                                                "", 20);
                                        MultiLanguage.broadcastSound(Sound.NOTE_PLING);
                                    } else if (Data.waittomove_counter == 1) {
                                        MultiLanguage.broadcastTitle(
                                                "§61",
                                                "",
                                                "§61",
                                                "",
                                                "§61",
                                                "", 20);
                                        MultiLanguage.broadcastSound(Sound.NOTE_PLING);
                                    }
                                }
                            }, 20, 20);
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                StatsManager.addGame(player.getUniqueId());
                            }
                            ItemDropManager.start();
                        }
                    }, 0, 20);
                }
            }
        } else {
            Data.spectators.add(p);
            e.setJoinMessage(null);
            p.setAllowFlight(true);
            p.spigot().setCollidesWithEntities(false);
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            p.getInventory().setItem(4, new ItemAPI("§bNavigator", Material.COMPASS, (byte) 0, 1, null).build());
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!Data.spectators.contains(player)) {
                    player.hidePlayer(p);
                }
            }
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                for (Player player : teamObject.getPlayers()) {
                    p.teleport(player);
                    break;
                }
                break;
            }
            ScoreboardObject.setSideboard();
        }
    }

    private void setDefaultStuff(Player p) {
        p.setHealth(20);
        p.setFoodLevel(20);
        p.setLevel(0);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        if (Data.lobby_counter >= 10) {
            if (Data.language == Language.DE) {
                p.getInventory().setItem(1, new ItemAPI("§bTeam wählen...", Material.BOOK, (byte) 0, 1, null).build());
                p.getInventory().setItem(7, new ItemAPI("§bErfolge...", Material.NETHER_STAR, (byte) 0, 1, null).build());
            } else if (Data.language == Language.EN) {
                p.getInventory().setItem(1, new ItemAPI("§bSelect team...", Material.BOOK, (byte) 0, 1, null).build());
                p.getInventory().setItem(7, new ItemAPI("§bArchivements...", Material.NETHER_STAR, (byte) 0, 1, null).build());
            } else if (Data.language == Language.ES) {
                p.getInventory().setItem(1, new ItemAPI("§bSelecciona un equipo...", Material.BOOK, (byte) 0, 1, null).build());
                p.getInventory().setItem(7, new ItemAPI("§bLogros...", Material.NETHER_STAR, (byte) 0, 1, null).build());
            }
        }
    }

    private void spawnHolo(Player p) {
        Hologram holo = HologramsAPI.createHologram(BridgeFighters.getInstance(), TopTenSigns.stats_holo);
        VisibilityManager visibilityManager = holo.getVisibilityManager();
        visibilityManager.showTo(p);
        visibilityManager.setVisibleByDefault(false);
        MultiLanguage.appendHoloLine(holo, p,
                "§a§lSTATISTIKEN",
                "§a§lSTATS",
                "§a§lESTADÍSTICAS");
        holo.appendTextLine("");
        MultiLanguage.appendHoloLine(holo, p,
                "§7§lRang §6§l» #" + StatsManager.getRang(p.getUniqueId()),
                "§7§lRank §6§l» #" + StatsManager.getRang(p.getUniqueId()),
                "§7§lRango §6§l» #" + StatsManager.getRang(p.getUniqueId()));
        holo.appendTextLine("");
        MultiLanguage.appendHoloLine(holo, p,
                "§7§lPunkte §6§l» " + StatsManager.getPoints(p.getUniqueId()),
                "§7§lPoints §6§l» " + StatsManager.getPoints(p.getUniqueId()),
                "§7§lPuntos §6§l» " + StatsManager.getPoints(p.getUniqueId()));
        holo.appendTextLine("");
        holo.appendTextLine("§7§lKD §6§l» " + StatsManager.getKD(p.getUniqueId()));
        holo.appendTextLine("");
        MultiLanguage.appendHoloLine(holo, p,
                "§7§lGewonnen §6§l» " + StatsManager.getWonGames(p.getUniqueId()),
                "§7§lWon §6§l» " + StatsManager.getWonGames(p.getUniqueId()),
                "§7§lGanados §6§l» " + StatsManager.getWonGames(p.getUniqueId()));
        holo.appendTextLine("");
        MultiLanguage.appendHoloLine(holo, p,
                "§7§lVerloren §6§l» " + StatsManager.getLostGames(p.getUniqueId()),
                "§7§lLost §6§l» " + StatsManager.getLostGames(p.getUniqueId()),
                "§7§lPerdidos §6§l» " + StatsManager.getLostGames(p.getUniqueId()));
    }

    private static void spawnSigns() {
        for (Map.Entry<Integer, Location> entry : TopTenSigns.alltime.entrySet()) {
            UUID playeruuid = StatsManager.getPlayerWithRang(entry.getKey());
            if (playeruuid != null) {
                Sign sign = (Sign) entry.getValue().getBlock().getState();
                String ext;
                String color;
                switch (entry.getKey()) {
                    case 1: {
                        ext = "st";
                        color = "§6";
                        break;
                    }
                    case 2: {
                        ext = "nd";
                        color = "§7";
                        break;
                    }
                    case 3: {
                        ext = "rd";
                        color = "§c";
                        break;
                    }
                    default: {
                        ext = "th";
                        color = "§8";
                    }
                }
                sign.setLine(0, color + entry.getKey() + ext + " Place");
                sign.setLine(2, "§8" + StatsManager.getOfflineName(playeruuid));
                sign.setLine(3, "§8" + StatsManager.getPoints(playeruuid) + " Points");
                sign.update();
            }
        }
    }


    private static void spawnHeads() {
        for (Map.Entry<Integer, Location> entry : TopTenSigns.alltime.entrySet()) {
            UUID playeruuid = StatsManager.getPlayerWithRang(entry.getKey());
            if (playeruuid != null) {
                Block block = entry.getValue().add(0, 1, 0).getBlock();
                block.setTypeIdAndData(Material.SKULL.getId(), (byte) 1, true);
                Skull skull = (Skull) block;
                skull.setSkullType(SkullType.PLAYER);
                skull.setOwner(StatsManager.getOfflineName(playeruuid));
                skull.update();
            }
        }
    }


}
