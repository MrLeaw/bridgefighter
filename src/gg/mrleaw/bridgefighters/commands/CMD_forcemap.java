//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.commands;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class CMD_forcemap implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equals("forcemap")) {
            if (commandSender instanceof Player) {
                if (BridgeFighters.getGameState() == GameState.LOBBY) {
                    if (Data.lobby_counter >= 10) {
                        Player player = (Player) commandSender;
                        if (!player.hasPermission("bf.forcemap")) {
                            return false;
                        }
                        Inventory inventory = Bukkit.createInventory(player, 18, "§bForcemap");
                        int counter = 0;
                        for (MapSettings mapSettings : BridgeFighters.getMaps()) {
                            inventory.setItem(counter, new ItemAPI(mapSettings.getMapname(), Material.MAP, (byte) 0, 1, null).build());
                            counter++;
                        }
                        player.openInventory(inventory);
                    } else {
                        MultiLanguage.sendMessage((Player) commandSender,
                                Data.getPrefix() + "§cDas Spiel beginnt in Kürze!",
                                Data.getPrefix() + "§cThe game starts soon!",
                                Data.getPrefix() + "§c¡El juego comienza pronto!");
                    }
                } else {
                    MultiLanguage.sendMessage((Player) commandSender,
                            Data.getPrefix() + "§cDas Spiel ist nicht in der Lobby-Phase!",
                            Data.getPrefix() + "§cThe game is not in the lobby phase!",
                            Data.getPrefix() + "§c¡El juego no está en la fase de lobby!");
                }
            } else {
                commandSender.sendMessage(Data.getPrefix() + "§cMan Marcel! Rly? Du musst ein Spieler sein!");
            }
        }
        return false;
    }

}
