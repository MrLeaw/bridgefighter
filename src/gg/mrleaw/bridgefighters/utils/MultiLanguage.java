//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import de.Herbystar.TTA.TTA_Methods;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class MultiLanguage {

    public static void sendMessage(Player player, String german, String english, String spanish) {
        if (Data.language == Language.DE) {
            player.sendMessage(german);
        } else if (Data.language == Language.EN) {
            player.sendMessage(english);
        } else if (Data.language == Language.ES) {
            player.sendMessage(spanish);
        }
    }

    public static void broadcastMessage(String german, String english, String spanish) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (Data.language == Language.DE) {
                player.sendMessage(german);
            } else if (Data.language == Language.EN) {
                player.sendMessage(english);
            } else if (Data.language == Language.ES) {
                player.sendMessage(spanish);
            }
        }
    }

    public static void sendTitle(Player player, String titlegerman, String subtitlegerman, String titleenglish, String subtitleenglish, String titlespanish, String subtitlespanish, int stay) {
        if (Data.language == Language.DE) {
            TTA_Methods.sendTitle(player, titlegerman, 5, stay, 5, subtitlegerman, 5, stay, 5);
        } else if (Data.language == Language.EN) {
            TTA_Methods.sendTitle(player, titleenglish, 5, stay, 5, subtitleenglish, 5, stay, 5);
        } else if (Data.language == Language.ES) {
            TTA_Methods.sendTitle(player, titlespanish, 5, stay, 5, subtitlespanish, 5, stay, 5);
        }
    }

    public static void broadcastTitle(String titlegerman, String subtitlegerman, String titleenglish, String subtitleenglish, String titlespanish, String subtitlespanish, int stay) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (Data.language == Language.DE) {
                TTA_Methods.sendTitle(player, titlegerman, 5, stay, 5, subtitlegerman, 5, stay, 5);
            } else if (Data.language == Language.EN) {
                TTA_Methods.sendTitle(player, titleenglish, 5, stay, 5, subtitleenglish, 5, stay, 5);
            } else if (Data.language == Language.ES) {
                TTA_Methods.sendTitle(player, titlespanish, 5, stay, 5, subtitlespanish, 5, stay, 5);
            }
        }
    }

    static double yoffset = 0;

    public static void sendHolo(Player player, Location location, String text, int duration) {
        yoffset = 0;
        Location location1 = location.add(0, 1, 0);
        Hologram hologram = HologramsAPI.createHologram(BridgeFighters.getInstance(), location1);
        hologram.appendTextLine(text);
        int task = Bukkit.getScheduler().scheduleAsyncRepeatingTask(BridgeFighters.getInstance(), () -> {
            yoffset += 0.0005;
            hologram.teleport(location1.add(0, yoffset, 0));
        }, 1, 1);
        Bukkit.getScheduler().runTaskLater(BridgeFighters.getInstance(), () -> {
            Bukkit.getScheduler().cancelTask(task);
            hologram.delete();
        }, duration);
    }

    public static void broadcastSound(Sound sound) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), sound, 10, 1);
        }
    }

    public static void sendActionBar(Player player, String textgerman, String textenglish, String textspanish) {
        if (Data.language == Language.DE) {
            TTA_Methods.sendActionBar(player, textgerman);
        } else if (Data.language == Language.EN) {
            TTA_Methods.sendActionBar(player, textenglish);
        } else if (Data.language == Language.ES) {
            TTA_Methods.sendActionBar(player, textspanish);
        }
    }

    public static void appendHoloLine(Hologram hologram, Player player, String textgerman, String textenglish, String textspanish) {
        if (Data.language == Language.DE) {
            hologram.appendTextLine(textgerman);
        } else if (Data.language == Language.EN) {
            hologram.appendTextLine(textenglish);
        } else if (Data.language == Language.ES) {
            hologram.appendTextLine(textspanish);
        }
    }

}
