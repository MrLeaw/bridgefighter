//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.utils.ItemAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;

import java.util.Map;

public class TeamShopListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        if (e.getClickedInventory() != null && e.getClickedInventory().getName().equals("§bShop")) {
            e.setCancelled(true);
            if (e.getCurrentItem().equals(new ItemAPI("Blocks", Material.SANDSTONE, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Armor", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Tools", Material.IRON_PICKAXE, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Swords", Material.GOLD_SWORD, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Food", Material.APPLE, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Chests", Material.CHEST, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Potions", Material.POTION, (byte) 0, 1, null).build()) ||
                    e.getCurrentItem().equals(new ItemAPI("Specials", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build())) {
                Inventory inventory = Bukkit.createInventory(e.getWhoClicked(), 4 * 9, "§bShop");
                inventory.setItem(0, new ItemAPI("Blocks", Material.SANDSTONE, (byte) 0, 1, null).build());
                inventory.setItem(1, new ItemAPI("Armor", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build());
                inventory.setItem(2, new ItemAPI("Tools", Material.IRON_PICKAXE, (byte) 0, 1, null).build());
                inventory.setItem(3, new ItemAPI("Swords", Material.GOLD_SWORD, (byte) 0, 1, null).build());
                inventory.setItem(4, new ItemAPI("Food", Material.APPLE, (byte) 0, 1, null).build());
                inventory.setItem(5, new ItemAPI("Chests", Material.CHEST, (byte) 0, 1, null).build());
                inventory.setItem(6, new ItemAPI("Potions", Material.POTION, (byte) 0, 1, null).build());
                inventory.setItem(7, new ItemAPI("Specials", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build());
                for (int i = 9; i < 18; i++) {
                    inventory.setItem(i, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 7, 1, null).build());
                }
                if (e.getCurrentItem().equals(new ItemAPI("Blocks", Material.SANDSTONE, (byte) 0, 1, null).build())) {
                    inventory.setItem(9, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.SANDSTONE, (byte) 0, 2, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.ENDER_STONE, (byte) 0, 1, null).build());
                    inventory.setItem(20, new ItemAPI("", Material.IRON_BLOCK, (byte) 0, 1, null).build());
                    inventory.setItem(21, new ItemAPI("", Material.ICE, (byte) 0, 1, null).build());
                    inventory.setItem(22, new ItemAPI("", Material.WATER_LILY, (byte) 0, 1, null).build());
                    inventory.setItem(23, new ItemAPI("", Material.SANDSTONE_STAIRS, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 1, null).build());
                    inventory.setItem(28, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 2, null).build());
                    inventory.setItem(30, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 4, null).build());
                    inventory.setItem(31, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                    inventory.setItem(32, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 3, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Armor", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build())) {
                    inventory.setItem(10, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.CHAINMAIL_HELMET, (byte) 0, 1, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build());
                    inventory.setItem(20, new ItemAPI("", Material.CHAINMAIL_LEGGINGS, (byte) 0, 1, null).build());
                    inventory.setItem(21, new ItemAPI("", Material.CHAINMAIL_BOOTS, (byte) 0, 1, null).build());
                    inventory.setItem(22, new ItemAPI("", Material.LEATHER_HELMET, (byte) 0, 1, null).build());
                    inventory.setItem(23, new ItemAPI("", Material.LEATHER_CHESTPLATE, (byte) 0, 1, null).build());
                    inventory.setItem(24, new ItemAPI("", Material.LEATHER_LEGGINGS, (byte) 0, 1, null).build());
                    inventory.setItem(25, new ItemAPI("", Material.LEATHER_BOOTS, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 5, null).build());
                    inventory.setItem(28, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 8, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 7, null).build());
                    inventory.setItem(30, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 6, null).build());
                    inventory.setItem(31, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(32, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 15, null).build());
                    inventory.setItem(33, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 12, null).build());
                    inventory.setItem(34, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 10, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Tools", Material.IRON_PICKAXE, (byte) 0, 1, null).build())) {
                    inventory.setItem(11, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.WOOD_PICKAXE, (byte) 0, 1, null, Enchantment.DIG_SPEED, 2).build());
                    inventory.setItem(19, new ItemAPI("", Material.IRON_PICKAXE, (byte) 0, 1, null, Enchantment.DIG_SPEED, 2).build());
                    inventory.setItem(20, new ItemAPI("", Material.DIAMOND_PICKAXE, (byte) 0, 1, null, Enchantment.DIG_SPEED, 2).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(28, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 6, null).build());
                    inventory.setItem(29, new ItemAPI("§6Gold", Material.GOLD_INGOT, (byte) 0, 3, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Swords", Material.GOLD_SWORD, (byte) 0, 1, null).build())) {
                    inventory.setItem(12, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.STICK, (byte) 0, 1, null, Enchantment.KNOCKBACK, 2).build());
                    inventory.setItem(19, new ItemAPI("", Material.GOLD_SWORD, (byte) 0, 1, null, Enchantment.DAMAGE_ALL, 1, Enchantment.DURABILITY, 10).build());
                    inventory.setItem(20, new ItemAPI("", Material.GOLD_SWORD, (byte) 0, 1, null, Enchantment.DAMAGE_ALL, 2, Enchantment.DURABILITY, 10).build());
                    inventory.setItem(21, new ItemAPI("", Material.IRON_SWORD, (byte) 0, 1, null, Enchantment.DAMAGE_ALL, 3, Enchantment.DURABILITY, 10).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(28, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 3, null).build());
                    inventory.setItem(30, new ItemAPI("§6Gold", Material.GOLD_INGOT, (byte) 0, 5, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Food", Material.APPLE, (byte) 0, 1, null).build())) {
                    inventory.setItem(13, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.APPLE, (byte) 0, 1, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.COOKED_BEEF, (byte) 0, 1, null).build());
                    inventory.setItem(20, new ItemAPI("", Material.CAKE, (byte) 0, 1, null).build());
                    inventory.setItem(21, new ItemAPI("", Material.GOLDEN_APPLE, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 1, null).build());
                    inventory.setItem(28, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 2, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                    inventory.setItem(30, new ItemAPI("§6Gold", Material.GOLD_INGOT, (byte) 0, 2, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Chests", Material.CHEST, (byte) 0, 1, null).build())) {
                    inventory.setItem(14, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.CHEST, (byte) 0, 1, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.ENDER_CHEST, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                    inventory.setItem(28, new ItemAPI("§6Gold", Material.GOLD_INGOT, (byte) 0, 1, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Potions", Material.POTION, (byte) 0, 1, null).build())) {
                    inventory.setItem(15, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", 1, null, PotionType.INSTANT_HEAL, true).buildPotion());
                    inventory.setItem(19, new ItemAPI("", 1, null, PotionType.SPEED, true).buildPotion());
                    inventory.setItem(20, new ItemAPI("", 1, null, PotionType.JUMP, true).buildPotion());
                    inventory.setItem(21, new ItemAPI("", 1, null, PotionType.INVISIBILITY, true).buildPotion());
                    inventory.setItem(22, new ItemAPI("", 1, null, PotionType.STRENGTH, true).buildPotion());
                    inventory.setItem(27, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 3, null).build());
                    inventory.setItem(28, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 7, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 16, null).build());
                    inventory.setItem(30, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 32, null).build());
                    inventory.setItem(31, new ItemAPI("§6Gold", Material.GOLD_INGOT, (byte) 0, 7, null).build());
                } else if (e.getCurrentItem().equals(new ItemAPI("Specials", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build())) {
                    inventory.setItem(16, new ItemAPI(" ", Material.STAINED_GLASS_PANE, (byte) 5, 1, null).build());
                    inventory.setItem(18, new ItemAPI("", Material.WEB, (byte) 0, 1, null).build());
                    inventory.setItem(19, new ItemAPI("", Material.FISHING_ROD, (byte) 0, 1, null).build());
                    inventory.setItem(20, new ItemAPI("", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build());
                    inventory.setItem(21, new ItemAPI("", Material.SNOW_BALL, (byte) 0, 1, null).build());
                    inventory.setItem(22, new ItemAPI("", Material.WATCH, (byte) 0, 1, null).build());
                    inventory.setItem(23, new ItemAPI("", Material.LADDER, (byte) 0, 1, null).build());
                    inventory.setItem(24, new ItemAPI("", Material.TNT, (byte) 0, 1, null).build());
                    inventory.setItem(25, new ItemAPI("", Material.WOOD_PLATE, (byte) 0, 1, null).build());
                    inventory.setItem(27, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 16, null).build());
                    inventory.setItem(28, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 4, null).build());
                    inventory.setItem(29, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 3, null).build());
                    inventory.setItem(30, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 8, null).build());
                    inventory.setItem(31, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 5, null).build());
                    inventory.setItem(32, new ItemAPI("§cBronze", Material.CLAY_BRICK, (byte) 0, 1, null).build());
                    inventory.setItem(33, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 2, null).build());
                    inventory.setItem(34, new ItemAPI("§7Iron", Material.IRON_INGOT, (byte) 0, 1, null).build());
                }
                e.getWhoClicked().openInventory(inventory);
            }
            if (e.getCurrentItem().getType().equals(Material.CLAY_BRICK) && e.getCurrentItem().getItemMeta().getDisplayName().equals("§cBronze")) {
                int amount = 0;
                for (ItemStack is : e.getWhoClicked().getInventory().all(Material.CLAY_BRICK).values()) {
                    amount = amount + is.getAmount();
                }
                boolean empty_stack = false;
                for (int i = 0; i < 4 * 9; i++) {
                    if (e.getWhoClicked().getInventory().getItem(i) == null) {
                        empty_stack = true;
                    }
                }
                if (!empty_stack) {
                    for (int i = 0; i < 4 * 9; i++) {
                        if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                            for (ItemStack is : e.getWhoClicked().getInventory().all(e.getClickedInventory().getItem(i - 9).getType()).values()) {
                                if (is.getAmount() != is.getMaxStackSize()) {
                                    empty_stack = true;
                                }
                            }
                        }
                    }
                }
                // Noch ein freier Slot im Inv?
                if (empty_stack) {
                    // War es ein Shift Click?
                    if (e.isShiftClick()) {
                        int maxstacksize = 0;
                        int amountperprice = 1;
                        for (int i = 0; i < 4 * 9; i++) {
                            if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                maxstacksize = e.getClickedInventory().getItem(i - 9).getMaxStackSize();
                                amountperprice = e.getClickedInventory().getItem(i - 9).getAmount();
                            }
                        }
                        // Hat man genug Geld um einen Stack zu kaufen?
                        if ((e.getCurrentItem().getAmount() * maxstacksize) / amountperprice <= amount) {
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getClickedInventory().getItem(i - 9).getType(), maxstacksize));
                                    consumeItem((Player) e.getWhoClicked(), (e.getCurrentItem().getAmount() * maxstacksize) / amountperprice, Material.CLAY_BRICK);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else if (Math.floor((double) amount / e.getCurrentItem().getAmount()) * amountperprice > 0) {
                            int itemstoadd = (int) Math.floor((double) amount / e.getCurrentItem().getAmount()) * amountperprice;
                            int price = (int) Math.floor((double) amount / e.getCurrentItem().getAmount());
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getClickedInventory().getItem(i - 9).getType(), itemstoadd));
                                    consumeItem((Player) e.getWhoClicked(), price, Material.CLAY_BRICK);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else {
                            MultiLanguage.sendMessage(player,
                                    Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                    Data.getPrefix() + "§cYou can not afford that!",
                                    Data.getPrefix() + "§cNo puedes permitirte eso!");
                            ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                        }
                    } else {
                        if (e.getCurrentItem().getAmount() <= amount) {
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(e.getClickedInventory().getItem(i - 9));
                                    consumeItem((Player) e.getWhoClicked(), e.getCurrentItem().getAmount(), Material.CLAY_BRICK);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else {
                            MultiLanguage.sendMessage(player,
                                    Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                    Data.getPrefix() + "§cYou can not afford that!",
                                    Data.getPrefix() + "§cNo puedes permitirte eso!");
                            ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                        }
                    }
                } else {
                    MultiLanguage.sendMessage(player,
                            Data.getPrefix() + "§cDein Inventar ist voll!",
                            Data.getPrefix() + "§cYour inventory is full!",
                            Data.getPrefix() + "§c¡Tu inventario está lleno!");
                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                }
            } else if (e.getCurrentItem().getType().equals(Material.IRON_INGOT) && e.getCurrentItem().getItemMeta().getDisplayName().equals("§7Iron")) {
                int amount = 0;
                for (ItemStack is : e.getWhoClicked().getInventory().all(Material.IRON_INGOT).values()) {
                    amount = amount + is.getAmount();
                }
                boolean empty_stack = false;
                for (int i = 0; i < 4 * 9; i++) {
                    if (e.getWhoClicked().getInventory().getItem(i) == null) {
                        empty_stack = true;
                    }
                }
                if (!empty_stack) {
                    for (int i = 0; i < 4 * 9; i++) {
                        if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                            for (ItemStack is : e.getWhoClicked().getInventory().all(e.getClickedInventory().getItem(i - 9).getType()).values()) {
                                if (is.getAmount() != is.getMaxStackSize()) {
                                    empty_stack = true;
                                }
                            }
                        }
                    }
                }
                // Noch ein freier Slot im Inv?
                if (empty_stack) {
                    // War es ein Shift Click?
                    if (e.isShiftClick()) {
                        int maxstacksize = 0;
                        int amountperprice = 1;
                        for (int i = 0; i < 4 * 9; i++) {
                            if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                maxstacksize = e.getClickedInventory().getItem(i - 9).getMaxStackSize();
                                amountperprice = e.getClickedInventory().getItem(i - 9).getAmount();
                            }
                        }
                        // Hat man genug Geld um einen Stack zu kaufen?
                        if ((e.getCurrentItem().getAmount() * maxstacksize) / amountperprice <= amount) {
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getClickedInventory().getItem(i - 9).getType(), maxstacksize));
                                    consumeItem((Player) e.getWhoClicked(), (e.getCurrentItem().getAmount() * maxstacksize) / amountperprice, Material.IRON_INGOT);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else if (Math.floor((double) amount / e.getCurrentItem().getAmount()) * amountperprice > 0) {
                            int itemstoadd = (int) Math.floor((double) amount / e.getCurrentItem().getAmount()) * amountperprice;
                            int price = (int) Math.floor((double) amount / e.getCurrentItem().getAmount());
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getClickedInventory().getItem(i - 9).getType(), itemstoadd));
                                    consumeItem((Player) e.getWhoClicked(), price, Material.IRON_INGOT);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else {
                            MultiLanguage.sendMessage(player,
                                    Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                    Data.getPrefix() + "§cYou can not afford that!",
                                    Data.getPrefix() + "§cNo puedes permitirte eso!");
                            ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                        }
                    } else {
                        if (e.getCurrentItem().getAmount() <= amount) {
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(e.getClickedInventory().getItem(i - 9));
                                    consumeItem((Player) e.getWhoClicked(), e.getCurrentItem().getAmount(), Material.IRON_INGOT);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else {
                            MultiLanguage.sendMessage(player,
                                    Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                    Data.getPrefix() + "§cYou can not afford that!",
                                    Data.getPrefix() + "§cNo puedes permitirte eso!");
                            ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                        }
                    }
                } else {
                    MultiLanguage.sendMessage(player,
                            Data.getPrefix() + "§cDein Inventar ist voll!",
                            Data.getPrefix() + "§cYour inventory is full!",
                            Data.getPrefix() + "§c¡Tu inventario está lleno!");
                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                }
            } else if (e.getCurrentItem().getType().equals(Material.GOLD_INGOT) && e.getCurrentItem().getItemMeta().getDisplayName().equals("§6Gold")) {
                int amount = 0;
                for (ItemStack is : e.getWhoClicked().getInventory().all(Material.GOLD_INGOT).values()) {
                    amount = amount + is.getAmount();
                }
                boolean empty_stack = false;
                for (int i = 0; i < 4 * 9; i++) {
                    if (e.getWhoClicked().getInventory().getItem(i) == null) {
                        empty_stack = true;
                    }
                }
                if (!empty_stack) {
                    for (int i = 0; i < 4 * 9; i++) {
                        if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                            for (ItemStack is : e.getWhoClicked().getInventory().all(e.getClickedInventory().getItem(i - 9).getType()).values()) {
                                if (is.getAmount() != is.getMaxStackSize()) {
                                    empty_stack = true;
                                }
                            }
                        }
                    }
                }
                // Noch ein freier Slot im Inv?
                if (empty_stack) {
                    // War es ein Shift Click?
                    if (e.isShiftClick()) {
                        int maxstacksize = 0;
                        int amountperprice = 1;
                        for (int i = 0; i < 4 * 9; i++) {
                            if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                maxstacksize = e.getClickedInventory().getItem(i - 9).getMaxStackSize();
                                amountperprice = e.getClickedInventory().getItem(i - 9).getAmount();
                            }
                        }
                        // Hat man genug Geld um einen Stack zu kaufen?
                        if ((e.getCurrentItem().getAmount() * maxstacksize) / amountperprice <= amount) {
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getClickedInventory().getItem(i - 9).getType(), maxstacksize));
                                    consumeItem((Player) e.getWhoClicked(), (e.getCurrentItem().getAmount() * maxstacksize) / amountperprice, Material.GOLD_INGOT);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else if (Math.floor((double) amount / e.getCurrentItem().getAmount()) * amountperprice > 0) {
                            int itemstoadd = (int) Math.floor((double) amount / e.getCurrentItem().getAmount()) * amountperprice;
                            int price = (int) Math.floor((double) amount / e.getCurrentItem().getAmount());
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(new ItemStack(e.getClickedInventory().getItem(i - 9).getType(), itemstoadd));
                                    consumeItem((Player) e.getWhoClicked(), price, Material.GOLD_INGOT);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else {
                            MultiLanguage.sendMessage(player,
                                    Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                    Data.getPrefix() + "§cYou can not afford that!",
                                    Data.getPrefix() + "§cNo puedes permitirte eso!");
                            ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                        }
                    } else {
                        if (e.getCurrentItem().getAmount() <= amount) {
                            for (int i = 0; i < 4 * 9; i++) {
                                if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                    e.getWhoClicked().getInventory().addItem(e.getClickedInventory().getItem(i - 9));
                                    consumeItem((Player) e.getWhoClicked(), e.getCurrentItem().getAmount(), Material.GOLD_INGOT);
                                    ((Player) e.getWhoClicked()).updateInventory();
                                }
                            }
                        } else {
                            MultiLanguage.sendMessage(player,
                                    Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                    Data.getPrefix() + "§cYou can not afford that!",
                                    Data.getPrefix() + "§cNo puedes permitirte eso!");
                            ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                        }
                    }
                } else {
                    MultiLanguage.sendMessage(player,
                            Data.getPrefix() + "§cDein Inventar ist voll!",
                            Data.getPrefix() + "§cYour inventory is full!",
                            Data.getPrefix() + "§c¡Tu inventario está lleno!");
                    ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                }
            } else if (e.getCurrentItem().getType().equals(Material.STAINED_GLASS_PANE) && e.getCurrentItem().getItemMeta().getDisplayName().equals(" ")) {

            } else {
                if (!(e.getCurrentItem().equals(new ItemAPI("Blocks", Material.SANDSTONE, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Armor", Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Tools", Material.IRON_PICKAXE, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Swords", Material.GOLD_SWORD, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Bows", Material.BOW, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Food", Material.APPLE, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Chests", Material.CHEST, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Potions", Material.POTION, (byte) 0, 1, null).build()) ||
                        e.getCurrentItem().equals(new ItemAPI("Specials", Material.FLINT_AND_STEEL, (byte) 0, 1, null).build()))) {
                    for (int i = 0; i < 4 * 9; i++) {
                        if (e.getClickedInventory().getItem(i) != null && e.getClickedInventory().getItem(i).equals(e.getCurrentItem())) {
                            int amount = 0;
                            for (ItemStack is : e.getWhoClicked().getInventory().all(e.getClickedInventory().getItem(i + 9).getType()).values()) {
                                amount = amount + is.getAmount();
                            }
                            if (e.getClickedInventory().getItem(i + 9).getAmount() <= amount) {
                                ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ITEM_PICKUP, 10, 1);
                                e.getWhoClicked().getInventory().addItem(e.getCurrentItem());
                                consumeItem((Player) e.getWhoClicked(), e.getClickedInventory().getItem(i + 9).getAmount(), e.getClickedInventory().getItem(i + 9).getType());
                                ((Player) e.getWhoClicked()).updateInventory();
                            } else {
                                MultiLanguage.sendMessage(player,
                                        Data.getPrefix() + "§cDas kannst du dir nicht leisten!",
                                        Data.getPrefix() + "§cYou can not afford that!",
                                        Data.getPrefix() + "§cNo puedes permitirte eso!");
                                ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.LAVA_POP, 10, 1);
                            }
                        }
                    }
                }
            }
        } else if (e.getClickedInventory() != null && e.getClickedInventory().equals(e.getWhoClicked().getInventory()) && e.getWhoClicked().getOpenInventory().getTopInventory().getName().equals("§bShop") && e.isShiftClick()) {
            e.setCancelled(true);
        }
    }

    public static boolean consumeItem(Player player, int count, Material mat) {
        Map<Integer, ? extends ItemStack> ammo = player.getInventory().all(mat);
        int found = 0;
        for (ItemStack stack : ammo.values())
            found += stack.getAmount();
        if (count > found)
            return false;
        for (Integer index : ammo.keySet()) {
            ItemStack stack = ammo.get(index);
            int removed = Math.min(count, stack.getAmount());
            count -= removed;
            if (stack.getAmount() == removed)
                player.getInventory().setItem(index, null);
            else
                stack.setAmount(stack.getAmount() - removed);
            if (count <= 0)
                break;
        }
        player.updateInventory();
        return true;
    }

}