//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.utils.ScoreboardObject;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import gg.mrleaw.bridgefighters.utils.settings.Teams;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;

import java.util.ArrayList;

public class InventoryClickListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getClickedInventory() != null) {
            if (e.getClickedInventory().getName().equals("§bSelect team...") || e.getClickedInventory().getName().equals("§bTeam wählen...") || e.getClickedInventory().getName().equalsIgnoreCase("§bSelecciona un equipo...")) {
                e.setCancelled(true);
                if (e.getCurrentItem().getType() == Material.STAINED_CLAY) {
                    byte data = e.getCurrentItem().getData().getData();
                    TeamColor teamColor = TeamColor.BLACK;
                    for (TeamColor all_teamColor : TeamColor.values()) {
                        if (all_teamColor.getData() == data) {
                            teamColor = all_teamColor;
                        }
                    }
                    ArrayList<TeamObject> teamObjects = new ArrayList<>();
                    for (TeamObject teamObject : BridgeFighters.teamObjects) {
                        if (teamObject.getTeamColor() == teamColor) {
                            if (teamObject.getPlayers().size() < Teams.getTeam_size()) {
                                e.getWhoClicked().closeInventory();
                                MultiLanguage.sendMessage((Player) e.getWhoClicked(),
                                        Data.getPrefix() + "Du bist Team " + teamColor.getColor() + teamColor.toString() + "§7 beigetreten!",
                                        Data.getPrefix() + "You joined team " + teamColor.getColor() + teamColor.toString() + "§7!",
                                        Data.getPrefix() + "¡Te uniste al equipo " + teamColor.getColor() + teamColor.toString() + "§7!");
                                ScoreboardObject.setTeam((Player) e.getWhoClicked(), teamObject.getTeamColor());
                                ((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.FIRE_IGNITE, 10, 1);
                                teamObject.getPlayers().add((Player) e.getWhoClicked());
                                teamObjects.add(teamObject);
                            }
                        } else {
                            teamObject.getPlayers().remove(e.getWhoClicked());
                            teamObjects.add(teamObject);
                        }
                    }
                }
            } else if (e.getClickedInventory().getName().equals("§bNavigator") || e.getClickedInventory().getName().equals("§bNavegador")) {
                e.setCancelled(true);
                String playername = e.getCurrentItem().getItemMeta().getDisplayName().substring(2);
                if (Bukkit.getPlayer(playername) != null) {
                    e.getWhoClicked().teleport(Bukkit.getPlayer(playername));
                    e.getWhoClicked().closeInventory();
                }
            }
        }
    }

}
