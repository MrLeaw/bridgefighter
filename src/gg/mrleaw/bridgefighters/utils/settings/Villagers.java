//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils.settings;

import org.bukkit.Location;

import java.util.ArrayList;

public class Villagers {

    private static ArrayList<Location> middle_villagers = new ArrayList<>();
    private static ArrayList<Location> team_villagers = new ArrayList<>();



    public static ArrayList<Location> getMiddle_villagers() {
        return middle_villagers;
    }

    public static void setMiddle_villagers(ArrayList<Location> middle_villagers) {
        Villagers.middle_villagers = middle_villagers;
    }

    public static ArrayList<Location> getTeam_villagers() {
        return team_villagers;
    }

    public static void setTeam_villagers(ArrayList<Location> team_villagers) {
        Villagers.team_villagers = team_villagers;
    }
}
