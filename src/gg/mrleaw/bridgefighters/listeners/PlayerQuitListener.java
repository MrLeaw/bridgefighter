//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import com.xxmicloxx.NoteBlockAPI.model.Song;
import com.xxmicloxx.NoteBlockAPI.songplayer.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.songplayer.SongPlayer;
import com.xxmicloxx.NoteBlockAPI.utils.NBSDecoder;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.*;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        ScoreboardObject.removePrefix(p);
        Data.gamePlayers.remove(p);
        Data.jumpNRunPlayers.remove(p);
        if (!Data.spectators.contains(p)) {
            event.setQuitMessage(Data.getPrefix() + "§7- " + p.getDisplayName());
        }
        Data.spectators.remove(p);
        if (BridgeFighters.getGameState() == GameState.LOBBY) {
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                teamObject.getPlayers().remove(p);
            }
        } else if (BridgeFighters.getGameState() == GameState.INGAME) {
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                teamObject.getPlayers().remove(p);
            }
            ArrayList<TeamColor> teamColors = new ArrayList<>();
            for (TeamObject teamObject : BridgeFighters.teamObjects) {
                if (teamObject.getPlayers().size() > 0) {
                    teamColors.add(teamObject.getTeamColor());
                }
            }
            if (teamColors.size() == 1) {
                if (!Bukkit.getScheduler().isCurrentlyRunning(Data.restart_task)) {
                    Bukkit.getScheduler().runTaskLater(BridgeFighters.getInstance(), () -> {
                        BridgeFighters.setGameState(GameState.ENDING);
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            player.setGameMode(GameMode.ADVENTURE);
                            player.teleport(BridgeFighters.getGameSettings().getLobby_spawn());
                            player.getInventory().clear();
                            MultiLanguage.sendTitle(player,
                                    "§7Team " + teamColors.get(0).getColor() + teamColors.get(0).toString().substring(0, 1) + teamColors.get(0).toString().substring(1).toLowerCase(), "§7hat gewonnen!",
                                    "§7Team " + teamColors.get(0).getColor() + teamColors.get(0).toString().substring(0, 1) + teamColors.get(0).toString().substring(1).toLowerCase(), "§7has won!",
                                    "§7Equipo " + teamColors.get(0).getColor() + teamColors.get(0).toString().substring(0, 1) + teamColors.get(0).toString().substring(1).toLowerCase(), "§7ha ganado!", 90);
                        }
                        MultiLanguage.broadcastMessage(
                                Data.getPrefix() + "§7Team " + teamColors.get(0).getColor() + teamColors.get(0).toString().substring(0, 1) + teamColors.get(0).toString().substring(1).toLowerCase() + "§7 hat das Spiel gewonnen!",
                                Data.getPrefix() + "§7Team " + teamColors.get(0).getColor() + teamColors.get(0).toString().substring(0, 1) + teamColors.get(0).toString().substring(1).toLowerCase() + "§7 has won the game!",
                                Data.getPrefix() + "§7¡El equipo " + teamColors.get(0).getColor() + teamColors.get(0).toString().substring(0, 1) + teamColors.get(0).toString().substring(1).toLowerCase() + "§7 ha ganado el juego!");
                        for (TeamObject teamObject : BridgeFighters.teamObjects) {
                            if (teamObject.getTeamColor() == teamColors.get(0)) {
                                for (Player player : teamObject.getPlayers()) {
                                    StatsManager.addWonGame(player.getUniqueId());
                                    StatsManager.addPoints(player.getUniqueId(), 50);
                                    MultiLanguage.sendHolo(player, player.getLocation().add(player.getLocation().getDirection().multiply(2.0D).setY(0.0D)), "+ §6250 Coins", 50);
                                    player.playSound(player.getLocation(), Sound.LEVEL_UP, 10, 1);
                                    player.sendMessage(Data.getPrefix() + "+ §6250 Coins");
                                }
                            }
                        }
                        for (TeamObject teamObject : BridgeFighters.allPlayers) {
                            if (teamObject.getTeamColor() != teamColors.get(0)) {
                                for (Player player : teamObject.getPlayers()) {
                                    MultiLanguage.sendHolo(player, player.getLocation().add(player.getLocation().getDirection().multiply(2.0D).setY(0.0D)), "+ §650 Coins", 50);
                                    player.playSound(player.getLocation(), Sound.LEVEL_UP, 10, 1);
                                }
                            }
                        }
                        Bukkit.getScheduler().runTaskLater(BridgeFighters.getInstance(), () -> {
                            File folder = new File(BridgeFighters.getInstance().getDataFolder(), "endmusic");
                            File[] contents = folder.listFiles();
                            if(contents.length > 0) {
                                Song song = NBSDecoder.parse(contents[new Random().nextInt(contents.length)]);
                                SongPlayer players = new RadioSongPlayer(song);
                                players.setAutoDestroy(true);
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    players.addPlayer(player);
                                }
                                players.setPlaying(true);
                            }else {
                                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cNo song available. For end music, place .nbs files inside the endmusic folder.");
                            }
                        }, 20 * 2);
                        Data.restart_task = Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> {
                            switch (Data.restart_counter) {
                                case 1: {
                                    MultiLanguage.broadcastMessage(
                                            Data.getPrefix() + "§7Der Server wird in §61§7 Sekunde neu gestartet",
                                            Data.getPrefix() + "§7The server is restarted in §61§7 second",
                                            Data.getPrefix() + "§7El servidor se reinicia en §61§7 segundo.");
                                    break;
                                }
                                case 0: {
                                    MultiLanguage.broadcastMessage(
                                            Data.getPrefix() + "§7Der Server wird in §60§7 Sekunden neu gestartet",
                                            Data.getPrefix() + "§7The server is restarted in §60§7 seconds",
                                            Data.getPrefix() + "§7El servidor se reinicia en §60§7 segundos.");
                                    Bukkit.getConsoleSender().sendMessage("UNLOADING WORLDS");
                                    for(World world : Bukkit.getServer().getWorlds()){
                                        Bukkit.unloadWorld(world, false);
                                    }
                                    Bukkit.shutdown();
                                    break;
                                }
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 10:
                                case 15:
                                case 20:
                                case 30:
                                case 40:
                                case 50:
                                case 60:{
                                    MultiLanguage.broadcastMessage(
                                            Data.getPrefix() + "§7Der Server wird in §6" + Data.restart_counter + "§7 Sekunden neu gestartet",
                                            Data.getPrefix() + "§7The server will restart in §6" + Data.restart_counter + "§7 seconds",
                                            Data.getPrefix() + "§7El servidor se reinicia en §6" + Data.restart_counter + "§7 segundos.");
                                    break;
                                }
                                default: {

                                }
                            }
                            Data.restart_counter--;
                        }, 0, 20);

                    }, 5);
                }
            } else if (teamColors.size() == 0) {
                Bukkit.getConsoleSender().sendMessage("UNLOADING WORLDS");
                for(World world : Bukkit.getServer().getWorlds()){
                    Bukkit.unloadWorld(world, false);
                }
                Bukkit.shutdown();
            }
        }
    }

}
