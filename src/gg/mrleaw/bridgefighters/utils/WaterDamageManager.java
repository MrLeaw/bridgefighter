//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Boat;

public class WaterDamageManager {

    public static void start() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(BridgeFighters.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(player -> {
            if (BridgeFighters.getGameState() == GameState.INGAME) {
                Material playerblock = player.getLocation().getBlock().getType();
                if (playerblock == Material.WATER || playerblock == Material.STATIONARY_WATER) {
                    if (!Data.spectators.contains(player)) {
                        if (!player.isInsideVehicle()) {
                            if (player.getHealth() <= 3) {
                                Data.kill_reasons.put(player, "water");
                            }
                            player.damage(3);
                        } else {
                            if (!(player.getVehicle() instanceof Boat)) {
                                player.damage(3);
                            }
                        }
                    }
                    MultiLanguage.sendActionBar(player,
                            "§cVerlasse das Wasser, schnell!",
                            "§cLeave the water, fast!",
                            "§cDeja el agua, rápido!");
                }
            }
        }), 20, 20);
    }

}
