//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardObject implements Listener {

    public static Scoreboard sb;
    private static String finalteamname;

    public static void registerScoreboard() {
        sb = Bukkit.getScoreboardManager().getNewScoreboard();
        int counter = 1;
            int zeros = 6 - String.valueOf(counter).length();
            StringBuilder teamname = new StringBuilder();
            for (int i = 0; i < zeros; i++) {
                teamname.append("0");
            }
            teamname.append(counter);
            String groupname = "Players";
            groupname = groupname.substring(0, 1).toUpperCase() + groupname.substring(1);
            teamname.append(groupname);
            sb.registerNewTeam(teamname.toString());
            sb.getTeam(teamname.toString()).setPrefix("§7");
            finalteamname = teamname.toString();
            counter--;
        for (TeamObject teamObject : BridgeFighters.teamObjects) {
            String groupname2 = teamObject.getTeamColor().toString().toLowerCase();
            groupname2 = groupname2.substring(0, 1).toUpperCase() + groupname2.substring(1);
            sb.registerNewTeam(groupname2);
            sb.getTeam(groupname2).setPrefix(teamObject.getTeamColor().getColor());
        }
    }

    public static void registerSideboard() {
        Objective objective = sb.registerNewObjective("aaa", "bbb");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§bBridgeFighter");
        objective.getScore("   ").setScore(103);
        objective.getScore("§8GameID: §a000").setScore(102);
        objective.getScore(" ").setScore(101);
        for (TeamObject group : BridgeFighters.teamObjects) {
            if (group.getPlayers().size() > 0) {
                String groupname = group.getTeamColor().toString();
                groupname = groupname.substring(0, 1) + groupname.substring(1).toLowerCase();
                groupname = group.getTeamColor().getColor() + groupname;
                objective.getScore(groupname).setScore(100);
            }
        }
        objective.getScore("  ").setScore(-1);
    }

    public static void setPrefix(Player p) {
        sb.getTeam(finalteamname).addEntry(p.getName());
        Bukkit.getOnlinePlayers().forEach(all -> all.setScoreboard(sb));
    }

    public static void removePrefix(Player p) {
        sb.getTeam(finalteamname).removeEntry(p.getName());
        Bukkit.getOnlinePlayers().forEach(all -> all.setScoreboard(sb));
    }

    public static void setTeam(Player p, TeamColor teamColor) {
        String groupname = teamColor.toString().toLowerCase();
        groupname = groupname.substring(0, 1).toUpperCase() + groupname.substring(1);
        sb.getTeam(groupname).addEntry(p.getName());
        Bukkit.getOnlinePlayers().forEach(all -> all.setScoreboard(sb));
    }

    public static void setSideboard() {
        Bukkit.getOnlinePlayers().forEach(all -> all.setScoreboard(sb));
    }

}

