//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.commands;

import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.FileManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.IOException;

public class CMD_bfaccept implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof ConsoleCommandSender) {
            Data.cc_accepted = true;
            FileConfiguration cfg = FileManager.getMySQLFileConfiguration();
            cfg.set("cc_accepted", true);
            try {
                cfg.save(FileManager.getMySQLFile());
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§aYou have agreed to the CC license. To review it again, just turn cc_accepted off in the config.yml file.");
            } catch (IOException e) {
                Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + "§cUnable to save CC Message preferences. You can change them manually in the config.yml file.");
            }
        }
        return false;
    }
}
