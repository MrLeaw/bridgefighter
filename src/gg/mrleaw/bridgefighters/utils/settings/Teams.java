//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils.settings;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;

public class Teams {

    private static int team_count;
    private static int team_size;
    private static ArrayList<TeamColor> teams = new ArrayList<>();
    private static HashMap<TeamColor, Location> team_spawnpoints = new HashMap<>();
    private static HashMap<TeamColor, Location> team_withers = new HashMap<>();

    public static int getTeam_count() {
        return team_count;
    }

    public static void setTeam_count(int team_count) {
        Teams.team_count = team_count;
    }

    public static int getTeam_size() {
        return team_size;
    }

    public static void setTeam_size(int team_size) {
        Teams.team_size = team_size;
    }

    public static ArrayList<TeamColor> getTeams() {
        return teams;
    }

    public static void setTeams(ArrayList<TeamColor> teams) {
        Teams.teams = teams;
    }

    public static HashMap<TeamColor, Location> getTeam_spawnpoints() {
        return team_spawnpoints;
    }

    public static void setTeam_spawnpoints(HashMap<TeamColor, Location> team_spawnpoints) {
        Teams.team_spawnpoints = team_spawnpoints;
    }

    public static HashMap<TeamColor, Location> getTeam_withers() {
        return team_withers;
    }

    public static void setTeam_withers(HashMap<TeamColor, Location> team_withers) {
        Teams.team_withers = team_withers;
    }
}
