//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class TopTenSigns {

    public static Map<Integer, Location> alltime = new TreeMap<>();
    public static Map<Integer, Location> month = new TreeMap<>();
    public static Location stats_holo;

    public static void addAlltimeSign(int id, Location location) {
        alltime.put(id, location);
        FileConfiguration cfg = FileManager.getWaitingLobbyFileConfiguration();
        cfg.set("alltime", ConfigUtils.getIntLocationMapString(alltime));
        try {
            cfg.save(FileManager.getWaitingLobbyFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addMonthSign(int id, Location location) {
        month.put(id, location);
        FileConfiguration cfg = FileManager.getWaitingLobbyFileConfiguration();
        cfg.set("month", ConfigUtils.getIntLocationMapString(month));
        try {
            cfg.save(FileManager.getWaitingLobbyFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
