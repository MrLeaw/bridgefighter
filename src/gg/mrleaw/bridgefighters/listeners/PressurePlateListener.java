//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.*;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PressurePlateListener implements Listener {

    @EventHandler
    public void onPressurePlatePress(PlayerInteractEvent e) {
        if (BridgeFighters.getGameState() == GameState.LOBBY) {
            if (e.getAction() == Action.PHYSICAL) {
                if (e.getClickedBlock().getType() == Material.GOLD_PLATE) {
                    if (!Data.jumpNRunPlayers.contains(e.getPlayer())) {
                        Data.jumpNRunPlayers.add(e.getPlayer());
                        if (Data.language == Language.DE) {
                            e.getPlayer().getInventory().setItem(4, new ItemAPI("§cJumpNRun-Modus verlassen", Material.SLIME_BALL, (byte) 0, 1, null).build());
                        } else if (Data.language == Language.EN) {
                            e.getPlayer().getInventory().setItem(4, new ItemAPI("§cLeave JumpNRun-Mode", Material.SLIME_BALL, (byte) 0, 1, null).build());
                        } else if (Data.language == Language.ES) {
                            e.getPlayer().getInventory().setItem(4, new ItemAPI("§cSalir del modo JumpNRun", Material.SLIME_BALL, (byte) 0, 1, null).build());
                        }

                        MultiLanguage.sendMessage(e.getPlayer(),
                                Data.getPrefix() + "§aJumpNRun-Modus beigetreten!",
                                Data.getPrefix() + "§aJoined JumpNRun-Mode!",
                                Data.getPrefix() + "§aSe unió al modo JumpNRun!");
                        Data.jumpNRunLocation = e.getPlayer().getLocation();
                    }
                } else if (e.getClickedBlock().getType() == Material.IRON_PLATE) {
                    if (Data.jumpNRunPlayers.contains(e.getPlayer())) {
                        Data.jumpNRunPlayers.remove(e.getPlayer());
                        e.getPlayer().getInventory().setItem(4, new ItemStack(Material.AIR));
                        MultiLanguage.sendMessage(e.getPlayer(),
                                Data.getPrefix() + "§aHerzlichen Glückwunsch!",
                                Data.getPrefix() + "§aCongratulations!",
                                Data.getPrefix() + "§a¡Felicidades!");
                        e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.LEVEL_UP, 10, 1);
                        e.getPlayer().sendMessage(Data.getPrefix() + "+ §65 Coins");
                    }
                }
            }
        }
    }

}
