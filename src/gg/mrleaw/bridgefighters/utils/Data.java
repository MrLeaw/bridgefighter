//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class Data {

    public static int lobby_counter_task;
    public static Language language;
    public static Location jumpNRunLocation;
    public static boolean resetMaps;
    private static String prefix = "§bBridgeFighters §8» §7";
    private static Player setupPlayer;
    private static int setupCounter;
    private static SetupState setupState;
    public static boolean lobby_counter_running = false;
    public static int lobby_counter = 62;
    public static int lobby_min_to_start = 2;
    public static ArrayList<Player> gamePlayers = new ArrayList<>();
    public static ArrayList<Player> jumpNRunPlayers = new ArrayList<>();
    public static HashMap<TeamColor, Location> withers = new HashMap<>();
    public static ArrayList<Block> ingame_blocks = new ArrayList<>();
    public static HashMap<Player, String> kill_reasons = new HashMap<>();
    public static int restart_counter = 60;
    public static int restart_task;
    public static boolean waittomove = false;
    public static int waittomove_counter = 3;
    public static int waittomove_task = 3;
    public static HashMap<TeamColor, Player> last_wither_hitter = new HashMap<>();
    public static ArrayList<Player> spectators = new ArrayList<>();
    public static boolean cc_accepted;

    public static Player getSetupPlayer() {
        return setupPlayer;
    }

    public static SetupState getSetupState() {
        return setupState;
    }

    public static void setSetupState(SetupState setupState) {
        Data.setupState = setupState;
    }

    public static void setSetupPlayer(Player setupPlayer) {
        Data.setupPlayer = setupPlayer;
    }

    public static String getPrefix() {
        return prefix;
    }

    public static int getSetupCounter() {
        return setupCounter;
    }

    public static void setSetupCounter(int setupCounter) {
        Data.setupCounter = setupCounter;
    }
}
