//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.Daytime;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;

public class MapSettings {

    Daytime daytime;
    ArrayList<Location> middleVillagers;
    ArrayList<Location> teamVillagers;
    HashMap<TeamColor, Location> teamSpawnPoints;
    HashMap<TeamColor, Location> teamWitherSpawnPoints;
    ArrayList<Location> bronze;
    ArrayList<Location> iron;
    ArrayList<Location> gold;
    String mapname;
    String buildername;

    public MapSettings(Daytime daytime, ArrayList<Location> middleVillagers, ArrayList<Location> teamVillagers, HashMap<TeamColor, Location> teamSpawnPoints, HashMap<TeamColor, Location> teamWitherSpawnPoints, ArrayList<Location> bronze, ArrayList<Location> iron, ArrayList<Location> gold, String mapname, String buildername) {
        this.daytime = daytime;
        this.middleVillagers = middleVillagers;
        this.teamVillagers = teamVillagers;
        this.teamSpawnPoints = teamSpawnPoints;
        this.teamWitherSpawnPoints = teamWitherSpawnPoints;
        this.bronze = bronze;
        this.iron = iron;
        this.gold = gold;
        this.mapname = mapname;
        this.buildername = buildername;
    }

    public Daytime getDaytime() {
        return daytime;
    }

    public ArrayList<Location> getMiddleVillagers() {
        return middleVillagers;
    }

    public ArrayList<Location> getTeamVillagers() {
        return teamVillagers;
    }

    public HashMap<TeamColor, Location> getTeamSpawnPoints() {
        return teamSpawnPoints;
    }

    public HashMap<TeamColor, Location> getTeamWitherSpawnPoints() {
        return teamWitherSpawnPoints;
    }

    public ArrayList<Location> getBronze() {
        return bronze;
    }

    public ArrayList<Location> getIron() {
        return iron;
    }

    public ArrayList<Location> getGold() {
        return gold;
    }

    public String getMapname() {
        return mapname;
    }

    public String getBuildername() {
        return buildername;
    }
}
