//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import gg.mrleaw.bridgefighters.utils.settings.Daytime;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.Teams;
import gg.mrleaw.bridgefighters.main.BridgeFighters;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager {

    public static File getConfigFile(String filename) {
        return new File(BridgeFighters.getInstance().getDataFolder(), filename);
    }

    public static File getMySQLFile() {
        return new File(BridgeFighters.getInstance().getDataFolder(), "config.yml");
    }

    public static File getWaitingLobbyFile() {
        return new File(BridgeFighters.getInstance().getDataFolder(), "waitinglobby.yml");
    }

    public static FileConfiguration getMySQLFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getMySQLFile());
    }

    public static FileConfiguration getWaitingLobbyFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getWaitingLobbyFile());
    }

    public static FileConfiguration getConfigFileConfiguration(String filename) {
        return YamlConfiguration.loadConfiguration(getConfigFile(filename));
    }

    public static void setDefaultMySQLConfig() {
        FileConfiguration cfg = getMySQLFileConfiguration();
        cfg.options().copyDefaults(true);
        cfg.addDefault("host", "212.224.125.181");
        cfg.addDefault("user", "root");
        cfg.addDefault("password", "7SyVzAp9aN9NgjVm");
        cfg.addDefault("database", "bridgefighters");
        cfg.addDefault("port", 3306);
        cfg.addDefault("restart_counter", 62);
        cfg.addDefault("players_to_start", 2);
        cfg.addDefault("language", "EN");
        cfg.addDefault("cc_accepted", false);
        cfg.addDefault("reset_maps", false);
        try {
            cfg.save(getMySQLFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createDefaultSignValues() {
        FileConfiguration cfg = getWaitingLobbyFileConfiguration();
        cfg.options().copyDefaults(true);
        cfg.addDefault("alltime", "");
        cfg.addDefault("month", "");
        cfg.addDefault("stats_holo", "");
        try {
            cfg.save(getWaitingLobbyFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getSignValues() {
        FileConfiguration cfg = getWaitingLobbyFileConfiguration();
        if (!cfg.getString("alltime").equals("")) {
            TopTenSigns.alltime = ConfigUtils.getIntLocationMap(cfg.getString("alltime"));
        }
        if (!cfg.getString("month").equals("")) {
            TopTenSigns.month = ConfigUtils.getIntLocationMap(cfg.getString("month"));
        }
        if (!cfg.getString("stats_holo").equals("")) {
            TopTenSigns.stats_holo = ConfigUtils.getLocation(cfg.getString("stats_holo"));
        }
    }

    public static void getValues() {
        FileConfiguration cfg = getMySQLFileConfiguration();
        MySQL.host = cfg.getString("host");
        MySQL.username = cfg.getString("user");
        MySQL.password = cfg.getString("password");
        MySQL.database = cfg.getString("database");
        MySQL.port = cfg.getInt("port") + "";
        Data.restart_counter = cfg.getInt("restart_counter");
        Data.lobby_min_to_start = cfg.getInt("players_to_start");
        Data.language = Language.valueOf(cfg.getString("language"));
        Data.cc_accepted = cfg.getBoolean("cc_accepted");
        Data.resetMaps = cfg.getBoolean("reset_maps");
    }

    public static boolean isSetupCompleted(String filename) {
        FileConfiguration cfg = getConfigFileConfiguration(filename);
        return (cfg.isSet("daytime") &&
                cfg.isSet("teams.count") &&
                cfg.isSet("teams.size") &&
                cfg.isSet("teams.colors") &&
                cfg.isSet("teams.spawnpoints") &&
                cfg.isSet("teams.witherspawnpoints") &&
                cfg.isSet("villagers.middle") &&
                cfg.isSet("villagers.teams") &&
                cfg.isSet("spawners.bronze") &&
                cfg.isSet("spawners.iron") &&
                cfg.isSet("spawners.gold") &&
                cfg.isSet("map.name") &&
                cfg.isSet("map.builder") &&
                cfg.isSet("lobby.spawn"));
    }

    public static MapSettings readConfig(String filename) {
        FileConfiguration cfg = getConfigFileConfiguration(filename);

        Bukkit.createWorld(new WorldCreator(ConfigUtils.getWorldName(ConfigUtils.getWorldNameFromHashMap(cfg.getString("teams.witherspawnpoints")))));
        Bukkit.createWorld(new WorldCreator(ConfigUtils.getWorldName(cfg.getString("lobby.spawn"))));

        for(World world : Bukkit.getServer().getWorlds()){
            world.setAutoSave(false);
        }

        Daytime daytime = Daytime.valueOf(cfg.getString("daytime").toUpperCase());
        ArrayList<Location> middleVillagers = ConfigUtils.getLocationArrayList(cfg.getString("villagers.middle"));
        ArrayList<Location> teamVillagers = ConfigUtils.getLocationArrayList(cfg.getString("villagers.teams"));
        HashMap<TeamColor, Location> teamSpawnPoints = ConfigUtils.getTeamColorLocationHashMap(cfg.getString("teams.spawnpoints"));
        HashMap<TeamColor, Location> teamWitherSpawnPoints = ConfigUtils.getTeamColorLocationHashMap(cfg.getString("teams.witherspawnpoints"));
        ArrayList<Location> bronze = ConfigUtils.getLocationArrayList(cfg.getString("spawners.bronze"));
        ArrayList<Location> iron = ConfigUtils.getLocationArrayList(cfg.getString("spawners.iron"));
        ArrayList<Location> gold = ConfigUtils.getLocationArrayList(cfg.getString("spawners.gold"));
        String mapName = cfg.getString("map.name");
        String builderName = cfg.getString("map.builder");

        Location lobby_spawn = ConfigUtils.getLocation(cfg.getString("lobby.spawn"));
        int teamCount = ConfigUtils.getInt(cfg.getString("teams.count"));
        int teamSize = ConfigUtils.getInt(cfg.getString("teams.size"));
        ArrayList<TeamColor> teamColors = ConfigUtils.getTeamColorArrayList(cfg.getString("teams.colors"));

        MapSettings mapSettings = new MapSettings(daytime, middleVillagers, teamVillagers, teamSpawnPoints, teamWitherSpawnPoints, bronze, iron, gold, mapName, builderName);

        Teams.setTeam_count(teamCount);
        Teams.setTeam_size(teamSize);
        Teams.setTeams(teamColors);
        BridgeFighters.getGameSettings().setLobby_spawn(lobby_spawn);

        return mapSettings;
    }


    public static void readGameSettings() {
        File folder = new File("plugins/BridgeFighters");
        for (File file : folder.listFiles()) {
            String filename = file.getName();
            if (!filename.startsWith(".")) {
                if (!filename.equalsIgnoreCase("config.yml") && !filename.equalsIgnoreCase("waitinglobby.yml")) {
                    if (isSetupCompleted(filename)) {
                        readConfig(filename);
                        BridgeFighters.setGameState(GameState.LOBBY);
                        BridgeFighters.getMaps().add(readConfig(filename));
                    } else {
                        BridgeFighters.setGameState(GameState.SETUP);
                    }
                }
            }
        }
    }

}
















