//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.listeners;

import gg.mrleaw.bridgefighters.main.BridgeFighters;
import gg.mrleaw.bridgefighters.utils.Data;
import gg.mrleaw.bridgefighters.utils.MultiLanguage;
import gg.mrleaw.bridgefighters.utils.StatsManager;
import gg.mrleaw.bridgefighters.utils.settings.TeamColor;
import gg.mrleaw.bridgefighters.utils.settings.TeamObject;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.getEntity().spigot().respawn();
        if (Data.kill_reasons.containsKey(e.getEntity())) {
            e.setKeepInventory(true);
            e.getEntity().getInventory().clear();
            e.getEntity().getInventory().setArmorContents(null);
            if (Data.kill_reasons.get(e.getEntity()).equals("water")) {
                e.setDeathMessage(null);
                MultiLanguage.broadcastMessage(
                        Data.getPrefix() + "§7Der Spieler §6" + e.getEntity().getName() + "§7 ist im Wasser gestorben",
                        Data.getPrefix() + "§7The player §6" + e.getEntity().getName() + "§7 died in the water",
                        Data.getPrefix() + "§7El jugador §6" + e.getEntity().getName() + "§7 murió en el agua.");
                StatsManager.addDeath(e.getEntity().getUniqueId());
            } else if (Data.kill_reasons.get(e.getEntity()).equals("fall")) {
                e.setDeathMessage(null);
                MultiLanguage.broadcastMessage(
                        Data.getPrefix() + "§7Der Spieler §6" + e.getEntity().getName() + "§7 ist an Fallschaden gestorben",
                        Data.getPrefix() + "§7The player §6" + e.getEntity().getName() + "§7 died of falling damage",
                        Data.getPrefix() + "§7El jugador §6" + e.getEntity().getName() + "§7 murió de caída de daño.");
                StatsManager.addDeath(e.getEntity().getUniqueId());
            } else if (Data.kill_reasons.get(e.getEntity()).startsWith("player.")) {
                Player killer = Bukkit.getPlayer(Data.kill_reasons.get(e.getEntity()).replace("player.", ""));
                String color = "§4";
                double health = ((int) killer.getHealth()) / 2;
                if (health > 1) {
                    color = "§c";
                }
                if (health > 2) {
                    color = "§6";
                }
                if (health > 4) {
                    color = "§e";
                }
                if (health > 8) {
                    color = "§a";
                }
                TeamColor killercolor = TeamColor.BLACK;
                TeamColor killedcolor = TeamColor.BLACK;
                for (TeamObject teamObject : BridgeFighters.teamObjects) {
                    if (teamObject.getPlayers().contains(killer)) {
                        killercolor = teamObject.getTeamColor();
                    }else if (teamObject.getPlayers().contains(e.getEntity())) {
                        killedcolor = teamObject.getTeamColor();
                    }
                }
                e.setDeathMessage(null);
                MultiLanguage.broadcastMessage(
                        Data.getPrefix() + "§7Der Spieler " + killedcolor.getColor() + e.getEntity().getName() + "§7 wurde von " + killercolor.getColor() + killer.getName() + " §7 [" + color + health + "§c❤§7] getötet!",
                        Data.getPrefix() + "§7The player " + killedcolor.getColor() + e.getEntity().getName() + "§7 was killed by " + killercolor.getColor() + killer.getName() + " §7 [" + color + health + "§c❤§7]!",
                        Data.getPrefix() + "§7El jugador " + killedcolor.getColor() + e.getEntity().getName() + "§7 fue asesinado por " + killercolor.getColor() + killer.getName() + " §7 [" + color + health + "§c❤§7]!");
                boolean witheralive = false;
                for (World world : Bukkit.getWorlds()) {
                    for (Entity entity : world.getEntities()) {
                        if (entity.getType() == EntityType.WITHER && entity.getCustomName().equals(killedcolor.getColor() + killedcolor.toString())) {
                            witheralive = true;
                        }
                    }
                }
                if (!witheralive) {
                    MultiLanguage.sendHolo(killer, e.getEntity().getLocation(), "+ §625 Coins", 50);
                }
                killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 10, 1);
                StatsManager.addDeath(e.getEntity().getUniqueId());
                StatsManager.addKill(killer.getUniqueId());
                StatsManager.addPoints(killer.getUniqueId(), 5);
            }
            Data.kill_reasons.remove(e.getEntity());
        }
    }

}
