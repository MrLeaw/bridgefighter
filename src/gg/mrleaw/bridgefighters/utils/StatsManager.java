//  <=====================================[ Terms of usage ]=====================================>
//  This work is licensed under CC BY NC 4.0. You are free to:
//  - Share - copy and redistribute the material in any medium or format,
//  - Adapt - remix, transform, and build upon the material
//  Under the following terms:
//  - Attribution - You must give appropriate credit, provide a link to the license, and indicate if changes were made.
//    You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
//    You are not allowed to remove any attribution text from this code.
//    You are not allowed to change this terms.
//  - NonCommercial - You may not use the material for commercial purposes.
//  If you need more information about CC BY NC 4.0 visit https://creativecommons.org/licenses/by-nc/4.0/legalcode
//
//  This was developed by MrLeaw.
//  <=================[ Thanks for being interested in my work. It means a lot. ]=================>

package gg.mrleaw.bridgefighters.utils;

import org.bukkit.Bukkit;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class StatsManager {

    public static void checkIfNew(UUID uuid) {
        try {
            if (!MySQL.getResult("SELECT * FROM stats WHERE uuid = '" + uuid + "'").next()) {
                Bukkit.getOnlinePlayers().forEach(p -> {
                    if (p.getUniqueId() == uuid) {
                        new Thread(() -> MySQL.update("INSERT INTO stats (uuid, name, kills, deaths, points, wongames, games) VALUES('" + uuid + "','"+p.getName()+"',0,0,0,0,0)")).start();
                    }
                });
                checkIfNew(uuid);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addKill(UUID uuid) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET kills = kills + 1 WHERE uuid = '" + uuid + "'")).start();
    }

    public static String getOfflineName(UUID uuid) {
        checkIfNew(uuid);
        try {
            ResultSet rs = MySQL.getResult("SELECT name FROM stats WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void updateOfflineName(UUID uuid, String name) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET name = '" + name + "' WHERE uuid = '" + uuid + "'")).start();
    }

    public static void addDeath(UUID uuid) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET deaths = deaths + 1 WHERE uuid = '" + uuid + "'")).start();
    }

    public static void addPoints(UUID uuid, int points) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET points = points + " + points + " WHERE uuid = '" + uuid + "'")).start();
    }

    public static void removePoints(UUID uuid, int points) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET points = points - " + points + " WHERE uuid = '" + uuid + "'")).start();
    }

    public static void addWonGame(UUID uuid) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET wongames = wongames + 1 WHERE uuid = '" + uuid + "'")).start();
    }

    public static void addGame(UUID uuid) {
        checkIfNew(uuid);
        new Thread(() -> MySQL.update("UPDATE stats SET games = games + 1 WHERE uuid = '" + uuid + "'")).start();
    }

    public static int getKills(UUID uuid) {
        checkIfNew(uuid);
        try {
            ResultSet rs = MySQL.getResult("SELECT kills FROM stats WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getInt("kills");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getDeaths(UUID uuid) {
        checkIfNew(uuid);
        try {
            ResultSet rs = MySQL.getResult("SELECT deaths FROM stats WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getInt("deaths");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getPoints(UUID uuid) {
        checkIfNew(uuid);
        try {
            ResultSet rs = MySQL.getResult("SELECT points FROM stats WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getInt("points");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getWonGames(UUID uuid) {
        checkIfNew(uuid);
        try {
            ResultSet rs = MySQL.getResult("SELECT wongames FROM stats WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getInt("wongames");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getLostGames(UUID uuid) {
        return getGames(uuid) - getWonGames(uuid);
    }

    public static int getGames(UUID uuid) {
        checkIfNew(uuid);
        try {
            ResultSet rs = MySQL.getResult("SELECT games FROM stats WHERE uuid = '" + uuid + "'");
            while (rs.next()) {
                return rs.getInt("games");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getRang(UUID uuid) {
        checkIfNew(uuid);
        int place = 1;
        try {
            ResultSet rs = MySQL.getResult("SELECT * FROM `stats` ORDER BY `stats`.`points` DESC");
            while (rs.next()) {
                if (rs.getString("uuid").equals(uuid.toString())) {
                    return place;
                } else {
                    place++;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return place;
    }

    public static UUID getPlayerWithRang(int rang) {
        int place = 1;
        try {
            ResultSet rs = MySQL.getResult("SELECT * FROM `stats` ORDER BY `stats`.`points` DESC");
            while (rs.next()) {
                if (place == rang) {
                    return UUID.fromString(rs.getString("uuid"));
                } else {
                    place++;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static double getKD(UUID uuid) {
        double kd;
        double kills = getKills(uuid);
        double deaths = getDeaths(uuid);
        if (deaths != 0) {
            kd = kills / deaths;
            int kd100 = (int) (kd * 100);
            kd = kd100 / 100D;
        } else {
            kd = StatsManager.getKills(uuid);
        }
        return kd;
    }
}
